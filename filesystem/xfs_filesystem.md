### Filesystem:  XFS

-----
### Mục lục

- [1. Filesystem](#filesystem1)
	* [1.1 What is filesystem](#fs1.1)
	* [1.2 Terminology](#fs1.2)
	* [1.3 Tên mục](#chuong1.3)
- [2. Tên chương 2](#chuong2)
	* [2.1 Tên mục](#chuong2.1)

- [Tài liệu tham khảo](#example_ref)

-------

<a name="filesystem1"></a>
### **1. Filesystem**

<a name="fs1.1"></a>
#### **1.1 What is filesystem** 
> Filesystem -- is the way in which files are named and where they are placed logically for storage and retrieval  
- Trong máy tính, hệ thống tệp- là cách các tệp được đặt tên và vị trí đặt chúng một cách hợp lý để lưu trữ và truy xuất. Nếu không có hệ thống tệp, thông tin được lưu trữ sẽ không được tách biệt thành các tệp riêng lẻ và sẽ khó xác định và truy xuất. Khi dung lượng dữ liệu tăng lên, việc tổ chức và khả năng truy cập của các tệp riêng lẻ càng trở nên quan trọng hơn trong việc lưu trữ dữ liệu.   


<a name="fs1.2"></a>
#### **1.2 Terminology** 
> Disk     
![1](/uploads/c0083453c2968b8a47ae3d340beb950f/1.png)   

-  Là phương tiện lưu trữ có kích thước nhất định. 
- Hiện nay có 2 loại ổ cứng phổ biến là SSD và HDD, trong đó:
+ HDD là viết tắt của Hard Disk Drive hay ổ đĩa cứng truyền thống, dữ liệu được lưu trên các bề mặt phiến đĩa tròn làm bằng nhôm, thủy tinh hoặc gốm được phủ vật liệu từ tính. Tâm của đĩa có gắn một động cơ, khi hoạt động các tấm đĩa sẽ được quay bởi động cơ này để đọc ghi dữ liệu
+ SSD là viết tắt của Solid State Drive, tức ổ cứng thể rắn, ra đời như một giải pháp thay thế cho tốc độ chậm chạp của HDD truyền thống. Ổ  SSD sử dụng một tấm các ô điện để nhanh chóng gửi và nhận dữ liệu. Những tấm này được phân chia thành các phần được gọi là “trang” và là nơi lưu trữ dữ liệu. Các trang này được nhóm lại với nhau tạo thành các “khối”. SSD được gọi là ổ cứng thể rắn vì chúng không có bộ phận chuyển động.   

> Block   
  
- Đơn vị nhỏ nhất có thể ghi bởi đĩa hoặc hệ thống tệp. Mọi thứ mà hệ thống tệp thực hiện đều bao gồm các hoạt động được thực hiện trên các block. Một khối hệ thống tệp luôn có cùng kích thước bằng hoặc lớn hơn (theo bội số nguyên) so với block size ( Block size nhỏ nhất trong `xfs` là 512 bytes).  

> Partition    
một tập hợp con của  các khối trên đĩa. Một đĩa có thể có nhiều phân vùng. 
![1](/uploads/082c4694b738d8ae1db64524c80990b6/1.png) 

> Volume  

- Cũng giống như `partition`,  sự khác biệt chính giữa chúng là loại đĩa được sử dụng. Một ổ đĩa được tạo trên một đĩa động - một cấu trúc logic có thể mở rộng nhiều đĩa vật lý - trong khi một phân vùng được tạo trên một đĩa cơ bản.
 

<a name="chuong2"></a>
### **2. Tên chương 2**

<a name="chuong2.1"></a>
#### **2.1 Tên mục** 
> Ví dụ khi viết 1 định nghĩa 
- Nội dung của phần này 

<a name="example_ref"></a> 
### **Tài liệu tham khảo**
- Link tham khảo

+ FILE SYSTEM – A COMPONENT OF OPERATING SYSTEM, Brijender Kahanwal
