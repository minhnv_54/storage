# FIO

## Installation

- Centos
    - sudo yum install fio
- Ubuntu
    -  sudo apt install fio

## Use cases

#
Command
```
sudo fio --randrepeat=1 --ioengine=libaio --direct=1 --gtod_reduce=1 --name=test --filename=test --bs=4k --iodepth=64 --size=4G --readwrite=randrw --rwmixread=75
```
#
Parameters:
- --filename: abcyxz or /dev/sdX
- --size=4G
- TODO
