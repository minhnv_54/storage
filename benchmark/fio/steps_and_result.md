### Using FIO to measure disk I/O

**Benchmark environment**
```
[root@controller33 ceph]# ceph -s
  cluster:
    id:     4e3f311c-ad83-4660-b95a-b23df0c28e3d
    health: HEALTH_WARN
            too few PGs per OSD (10 < min 30)
 
  services:
    mon: 3 daemons, quorum controller33,controller34,controller35
    mgr: controller33(active), standbys: controller34, controller35
    osd: 35 osds: 35 up, 35 in
 
  data:
    pools:   1 pools, 128 pgs
    objects: 1.13 k objects, 4.1 GiB
    usage:   48 GiB used, 109 TiB / 109 TiB avail
    pgs:     128 active+clean
```

**Scenario:** Test on controller33 with virtual volume:
1. Combination of both Random reads and random writes **(75%-25%)**.
2.  **4 kilobyte** blocks. Again, databases and many other programs will read very small chunks of data - 4 kilobytes is a good working estimate.

**Steps**

Create pool and image
```
# ceph osd pool create donghm-pool 128
# ceph osd lspools
# rbd create donghm-pool/first_image --size 102400
# rbd ls -l -p donghm-pool
```
Activate the rbd kernel module
```
# modprobe rbd
# rbd feature disable first_image exclusive-lock object-map fast-diff deep-flatten -p donghm-pool
```
Map the **donghm-pool/first_image** image to a block device via rbd kernel module
```
# rbd map first_image -p donghm-pool
# rbd showmapped
id 	pool        	image       	snap 	device    
0  	donghm-pool 	first_image 	-    	/dev/rbd0
```
Format the disk and mount to a folder:
```
# mkfs.ext4 /dev/rbd0
# mkdir -p /mnt/donghm_disk
# mount /dev/rbd0 /mnt/donghm_disk/
# df -h
```
Install FIO
```
# yum install -y fio
# cd /mnt/donghm_disk/
```

**Command to test**
```
[root@controller33 donghm_disk]# fio --randrepeat=1 --ioengine=libaio --direct=1 \
--gtod_reduce=1 --name=test --filename=test --bs=4k --iodepth=64 --size=4G \
--readwrite=randrw --rwmixread=75
```

**Result when object-size is 4MB**

	[root@controller33 donghm_disk]# fio --randrepeat=1 --ioengine=libaio --direct=1 --gtod_reduce=1 --
	test: (g=0): rw=randrw, bs=(R) 4096B-4096B, (W) 4096B-4096B, (T) 4096B-4096B, ioengine=libaio, iodepth=64
	fio-3.1
	Starting 1 process
	test: Laying out IO file (1 file / 4096MiB)
	Jobs: 1 (f=1): [m(1)][100.0%][r=1469KiB/s,w=452KiB/s][r=367,w=113 IOPS][eta 00m:00s] 
	test: (groupid=0, jobs=1): err= 0: pid=38291: Mon Nov 12 06:37:33 2018
	  read: IOPS=832, BW=3329KiB/s (3409kB/s)(3070MiB/944333msec)
	   bw (  KiB/s): min= 1192, max= 5800, per=100.00%, avg=3331.63, stdev=501.19, samples=1888
	   iops        : min=  298, max= 1450, avg=832.87, stdev=125.29, samples=1888
	  write: IOPS=278, BW=1113KiB/s (1139kB/s)(1026MiB/944333msec)
	   bw (  KiB/s): min=  336, max= 1960, per=100.00%, avg=1113.40, stdev=171.99, samples=1888
	   iops        : min=   84, max=  490, avg=278.32, stdev=43.00, samples=1888
	  cpu          : usr=0.55%, sys=1.88%, ctx=972991, majf=0, minf=594
	  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=0.1%, 32=0.1%, >=64=100.0%
	     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
	     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.1%, >=64=0.0%
	     issued rwt: total=785920,262656,0, short=0,0,0, dropped=0,0,0
	     latency   : target=0, window=0, percentile=100.00%, depth=64

	Run status group 0 (all jobs):
	   READ: bw=3329KiB/s (3409kB/s), 3329KiB/s-3329KiB/s (3409kB/s-3409kB/s), io=3070MiB (3219MB), run=944333-944333msec
	  WRITE: bw=1113KiB/s (1139kB/s), 1113KiB/s-1113KiB/s (1139kB/s-1139kB/s), io=1026MiB (1076MB), run=944333-944333msec

	Disk stats (read/write):
	  rbd0: ios=785899/263029, merge=0/220, ticks=28574543/31335681, in_queue=59961855, util=100.00%

**Result when object-size is 8MB**

	test: (g=0): rw=randrw, bs=(R) 4096B-4096B, (W) 4096B-4096B, (T) 4096B-4096B, ioengine=libaio, iodepth=64
	fio-3.1
	Starting 1 process
	test: Laying out IO file (1 file / 4096MiB)
	Jobs: 1 (f=1): [m(1)][100.0%][r=1480KiB/s,w=432KiB/s][r=370,w=108 IOPS][eta 00m:00s] 
	test: (groupid=0, jobs=1): err= 0: pid=60879: Wed Nov 14 07:30:55 2018
	   read: IOPS=861, BW=3447KiB/s (3530kB/s)(3070MiB/911966msec)
	   bw (  KiB/s): min= 1272, max= 6232, per=100.00%, avg=3450.64, stdev=490.58, samples=1823
	   iops        : min=  318, max= 1558, avg=862.61, stdev=122.65, samples=1823
	  write: IOPS=288, BW=1152KiB/s (1180kB/s)(1026MiB/911966msec)
	   bw (  KiB/s): min=  368, max= 2120, per=100.00%, avg=1153.20, stdev=164.15, samples=1823
	   iops        : min=   92, max=  530, avg=288.25, stdev=41.03, samples=1823
	  cpu          : usr=0.57%, sys=1.93%, ctx=969582, majf=0, minf=589
	  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=0.1%, 32=0.1%, >=64=100.0%
	     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
	     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.1%, >=64=0.0%
	     issued rwt: total=785920,262656,0, short=0,0,0, dropped=0,0,0
	     latency   : target=0, window=0, percentile=100.00%, depth=64

	Run status group 0 (all jobs):
	   READ: bw=3447KiB/s (3530kB/s), 3447KiB/s-3447KiB/s (3530kB/s-3530kB/s), io=3070MiB (3219MB), run=911966-911966msec
	  WRITE: bw=1152KiB/s (1180kB/s), 1152KiB/s-1152KiB/s (1180kB/s-1180kB/s), io=1026MiB (1076MB), run=911966-911966msec

	Disk stats (read/write):
	  rbd1: ios=785915/263024, merge=0/214, ticks=27376661/30488957, in_queue=57912497, util=100.00%
