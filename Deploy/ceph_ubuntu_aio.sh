#!/bin/bash

# ENV
INTERFACE=ens3
DEV1=/dev/sdb
DEV2=/dev/sdbc

# Add ceph key and repo
wget -q -O- 'https://download.ceph.com/keys/release.asc' | sudo apt-key add -
echo deb https://download.ceph.com/debian-mimic/ $(lsb_release -sc) main | sudo tee /etc/apt/sources.list.d/ceph.list

# Create cluster folder
mkdir -p $HOME/cluster
cd $HOME/cluster
echo "$USER ALL = (root) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/$USER
sudo chmod 0440 /etc/sudoers.d/$USER

# Install ceph-ceploy
sudo apt update
sudo apt install ceph-deploy

# Prepare ssh from ceph-deploy to ceph-node
ssh-keygen
ssh-copy-id $USER@$HOSTNAME

touch ~/.ssh/config

cat << EOF >> ~/.ssh/config
Host $HOSTNAME
   Hostname $HOSTNAME
   User $USER
EOF

# Disable firewall
sudo iptables -A INPUT -i $INTERFACE -p tcp --dport 6789 -j ACCEPT
sudo iptables -A INPUT -i $INTERFACE -p tcp --dport 80 -j ACCEPT
sudo iptables -A INPUT -i $INTERFACE -p tcp --dport 69 -j ACCEPT

# Clean ceph-node
ceph-deploy purge $HOSTNAME
ceph-deploy purgedata $HOSTNAME
ceph-deploy forgetkeys
rm ceph.*

# Install ceph AIO
ceph-deploy new $HOSTNAME
ceph-deploy install $HOSTNAME --release mimic
cat << EOF >> ceph.conf
[global]
EOF
ceph-deploy mon create-initial
ceph-deploy admin $HOSTNAME
ceph-deploy mgr create $HOSTNAME
ceph-deploy osd create --data $DEV1 $HOSTNAME
ceph-deploy osd create --data $DEV2 $HOSTNAME

# Health check
sudo ceph health
