
Cài đặt sử dụng **prometheus_ceph_exporter**:
- Github source:  https://github.com/digitalocean/ceph_exporter
- Docker container source: https://hub.docker.com/r/digitalocean/ceph_exporter/
- Sử dụng Kolla-ansible để deploy 
- Grafana dasboard source: 
	- https://grafana.com/dashboards/917
	- https://grafana.com/dashboards/926
	- https://grafana.com/dashboards/923
- Tham khảo tại: https://computingforgeeks.com/monitoring-ceph-cluster-with-prometheus-and-grafana/ 


Cài đặt sử dụng **ceph mgr prometheus**:
- Cài thêm 2 plugin cho grafana: [grafana-piechart-panel](https://grafana.com/plugins/grafana-piechart-panel/installation) và [vonage-status-panel](https://grafana.com/plugins/vonage-status-panel)
- Enable prometheus trên ceph mgr: `ceph mgr enable prometheus`
- Kiểm tra lại: `netstat -anlp | grep 9283`
- Tạo file node_target.yml với nội dung:
	```
	[
      {
        "targets": [ "10.x.x.x:9283" ],
        "labels": {}
      }
	]
	```
- Chỉnh sửa `prometheus.yml` để thêm target của ceph mgr vào:
	```
	scrape_configs:
	  - ...
	  - job_name: 'ceph-mgr'
	    file_sd_configs:
	      - files:
	        - ceph_targets.yml
	```
- Restart lại prometheus_server container
- Dashboard sử dụng: https://github.com/ceph/ceph/tree/master/monitoring/grafana/dashboards
- Tham khảo tại: http://docs.ceph.com/docs/master/mgr/prometheus/ 


