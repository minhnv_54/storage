# Minimum recommended

- OSD
    - cpu: 1x 64-bit AMD-64/32-bit ARM dual-core
    - ram: 1GB per 1TB data
    - network: 2x 1GB Ethernet NICs
    - WAL: 1GB per daemon
    - RockDB: 1/100 of data size
- MON
    - cpu: 1x 64-bit AMD-64/32-bit ARM dual-core
    - ram: 1GB/1TB data
    - network: 2x 1GB Ethernet NICs
    - disk space: 10 GB per daemon
- MDS
    - cpu: 1x 64-bit AMD-64/32-bit ARM dual-core
    - ram: 1GB/1TB data
    - network: 2x 1GB Ethernet NICs
    - disk space: 1 GB per daemon
- RGW
    - cpu: 1x 64-bit AMD-64/32-bit ARM dual-core
    - ram: 1GB/1TB data
    - network: 2x 1GB Ethernet NICs
    - disk space: 5 GB per daemon

# Additional notes
