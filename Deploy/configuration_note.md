# Note for configuration

- When we deploy Ceph RadosGW, Ceph will automatically create *.rgw.* pool with default PG_NUM and we need PG_NUM is quite small >> `configure a small default PG_NUM and PGP_NUM`

```
[global]
	osd pool default pg num = 8
	osd pool default pgp num = 8
```
