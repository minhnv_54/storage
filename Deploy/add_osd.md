# Add OSDs

`Warning: The Ceph rebuild process can lead to slow or stuck requests from client!`
## Configuration

Init small weight for new OSD:

```
[global]
osd crush initial weight = 0.2
```

## Recommendation

1) Gradually increase weight

2) I would keep track of memory usage on the nodes to see if that increases under peering/backfilling,
  - If this is the case, and you’re using bluestore: try lowering bluestore_cache_size* params, to give you some leeway.

3) If using bluestore, try throttling by changing the following params, depending on your environment:
  - osd recovery sleep
  - osd recovery sleep hdd
  - osd recovery sleep ssd

4) QoS

## Check list

Thêm 3 nodes OSD với 35 disks (tất cả đều 4TB) vào Ceph cluster (các OSD khác nhau về kích cỡ: 2TB va 4TB)

- Clean các nodes osd hiện tại
	- Destroy ceph-cluster
	- Xóa lvm group trên các osd 
- Kiểm tra lại các cấu hình về mạng
- Benchmark và check bad blocks trên từng osd.
- Config trên cluster hiện tại để giảm ảnh hưởng tới Client I/O
	- Temporarily Disable Scrubbing (dự tính 3-5 OSDs thì enable lại scrub, sau đó lại disable)
		  `# ceph osd set noscrub`
		  `# ceph osd set nodeep-scrub`
	- Limit Backfill and Recovery (thay đổi runtime được) trong TH tác động lúc tải bình thường
	```sh
	osd_max_backfills = 1 (default)
	ceph tell osd.* config set osd_recovery_max_active 1
	ceph tell osd.1 config set osd_recovery_op_priority 1
    ```
    - Tăng Backfill and Recovery được trong TH tác động đêm, tải thấp
	```sh
	ceph tell 'osd.*' injectargs '--osd-max-backfills 64'
    ceph tell 'osd.*' injectargs '--osd-recovery-max-active 32'
    ```

- Chiến lược: thêm từng osd trên từng node osd
	- Provision the node
	- Thêm từng osd vào cluster, chờ cho tới khi cluster đạt trạng thái `active+clean` thì mới thêm OSD mới.
		- Khi thêm: đặt weigh cho OSD mới = `1.81940` để Ceph rebalance data sang ổ mới này ngang với các ổ 2TB hiện có trên cluster, tránh hiện tượng khi thêm OSD mới vào thì số PGs cho OSD đó cao hơn rất nhiều so với các OSDs khác đang trong hệ thống (theo bài test tại [đây](http://10.240.173.26:10080/cloud-team/ceph-deployment/blob/donghm/ceph-overview/tests/test_pgs.md))
- Refactor các config đã set
	- Kiểm tra trạng thái của cluster hiện tại: `active + clean` thì mới thực hiện các bước dưới
	- Unset  `noscrub` và `nodeep-scrub`

# Step-by-step

**Cấu hình trước khi add osd**
```
# ceph osd set noscrub
# ceph osd set nodeep-scrub
# ceph tell 'osd.*' injectargs '--osd-max-backfills 64'
# ceph tell 'osd.*' injectargs '--osd-recovery-max-active 32'
```
Chỉnh sửa trong file `/home/cephuser/cluster/ceph.conf` để set default weight cho các osd mới là 	`0.5`:
```
[global]
osd crush initial weight = 0.5
```

**Thêm OSD**:
- Add 9 OSD trên 3 nodes 1 lần (mỗi node 3 OSDs)
- Inject  `--osd-max-backfills 64` và `--osd-recovery-max-active 32` cho 9 OSDs mới add vào
- Chờ cho status của tất cả các PGs về trạng thái `active+clean` thì mới add thêm 9 OSDs mới.

**Tăng osd crush weight từ từ sau khi đã add hết tất cả OSDs:**

- Đầu tiên tăng lên weight thành **1** cho tất cả các OSD đã được thêm vào, chờ cho tới khi PGs status là `active+clean`
- Tăng tiếp lên thành **1.8** để đồng bộ với các ổ 2TB, chờ cho tới khi PGs status là `active+clean`

`Lặp lại 03 bước tiếp theo cho tới khi đạt được kết quả PG mong muốn trên các pool`

**1. Tạm dừng việc backfill**

```sh
ceph osd set norecover
ceph osd set nobackfill
ceph osd set nodown
ceph osd set noout
```

**2. Tăng PGs cho các Pool: mỗi lần tăng thêm 128 PGs**
- Tăng `pg_num` trước
- Chờ khoảng 30s cho các PGs mới được tạo và peering 
- Tăng `pgp_num` bằng với giá trị của pg_num
- Chờ cho tới khi pg status là `active+clean`
- Lặp lại các bước trên cho tới khi đủ số lượng PGs cần tăng (tuy nhiên không nên tăng quá 512 pg).

**3. Cho phép backfill trở lại vào các pg mới đã active**

```sh
ceph osd unset noout
ceph osd unset nodown
ceph osd unset nobackfill
ceph osd unset norecover
```

`Sau khi đã thành thành bước tăng PG, hiệu chỉnh lại các thông số`

**Chỉnh lại config cho backfills và recover-max-active về default**
```
# ceph tell 'osd.*' injectargs '--osd-max-backfills 1'
# ceph tell 'osd.*' injectargs '--osd-recovery-max-active 3'
```

**Unset noscub và nodeep-scrub**: cần thực hiện ban đêm để tránh ảnh hưởng tới client IO
```
# ceph osd unset noscrub  
# ceph osd unset nodeep-scrub
```

## Refers
- http://lists.ceph.com/pipermail/ceph-users-ceph.com/2018-April/026083.html
- http://lists.ceph.com/pipermail/ceph-users-ceph.com/2016-February/007591.html
- [Red Hat - Add/remove osd nodes](https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/2/html/administration_guide/adding_and_removing_osd_nodes)
- [Red Hat - Add an OSD](https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/2/html/administration_guide/managing_cluster_size#adding_an_osd)
