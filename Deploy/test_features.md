# Tested features

|     Feature     |      OS      | YES/NO |     Notes    |
|:---------------:|:------------:|:------:|:------------:|
| Resize instance |   Centos 7   |    1   | After reboot |
|                 | Ubuntu 16.04 |    1   | After reboot |
|                 |              |        |              |
