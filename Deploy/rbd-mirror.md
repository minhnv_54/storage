**Mô hình**:

- Cụm **1**: là `primary cluster` (còn gọi là `local cluster`), có **journal_pool**
- Cụm **2**: là `backup cluster` (còn gọi là `remote cluster`), có **rbd-mirro daemon**

Set cluster name tại 1 node MON trên **primary cluster**
```
# vim /etc/sysconfig/ceph
		CLUSTER=local
		CLUSTER=remote
# cd /etc/ceph/
# ln -s ceph.conf local.conf
# cp ceph.client.admin.keyring local.client.admin.keyring
# ceph -s --cluster local
```

Set cluster name tại 1 node MON trên **backup cluster**
```
# vim /etc/sysconfig/ceph
		CLUSTER=local
		CLUSTER=remote
# cd /etc/ceph/
# ln -s ceph.conf remote.conf
# cp ceph.client.admin.keyring remote.client.admin.keyring
# ceph -s --cluster local
```

Copy `ceph.conf` và `keyring` giữa 2 cluster với nhau
```
# scp /etc/ceph/local.conf <user>@<remote_mon-host-name>:/etc/ceph/
# scp /etc/ceph/local.client.local.keyring <user>@<remote_mon-host-name>:/etc/ceph/

# scp /etc/ceph/remote.conf <user>@<local_client-host-name>:/etc/ceph/
# scp /etc/ceph/remote.client.local.keyring <user>@<local_client-host-name>:/etc/ceph/
```
Trên **remote cluster**, thực hiện phân quyền các file keyring để `rbd-mirror daemon` có thể đọc được:
```
# chown ceph:ceph local.client.admin.keyring local.conf remote.client.admin.keyring remote.conf
```

Kiểm tra lại bằng cách đứng trên cả 2 node MON của cả 2 cluster và thực hiện;
```
# ceph -s --cluster=local
# ceph -s --cluster=remote
```

Tạo các pool tương ứng trên **remote cluster**
```sh
ceph osd pool create volumes 32
ceph osd pool set volumes size 2
ceph osd pool create images 32
ceph osd pool set images size 2
ceph osd pool create backups 32
ceph osd pool set backups size 2
ceph osd pool create vms 32
ceph osd pool set vms size 2
```

Enable `rbd apllication` cho các pool đã tạo:
```
rbd pool init vms
rbd pool init images
rbd pool init volumes
rbd pool init backups
```

### Tạo và config journal pool để chứa journal image trên primary cluster:
```
# ceph osd pool create journal_pool 32
# rbd pool init journal_pool
```
Config trong `ceph.conf` trên `local cluster`:
```
[global]
rbd journal pool = journal_pool

rbd default features = 125
```
Inject config cho mon.*
```
ceph tell mon.* injectargs '--rbd-default-features=125'
```
**Note**: Có warning `(not observed, change may require restart)` nhưng config vẫn thay đổi!!!

### Run rbd-mirror daemon on remote cluster
```
# yum install rbd-mirror
# systemctl enable ceph-rbd-mirror.target
# systemctl enable ceph-rbd-mirror@admin
# systemctl start ceph-rbd-mirror@admin
```

**Enable mirroring** trên các pool ở cả **2 cluster local và remote**
```
# rbd mirror pool enable volumes pool
# rbd mirror pool enable vms pool
# rbd mirror pool enable backups pool
# rbd mirror pool enable images pool
```

Trên **remote cluster** tạo các **pool peer** tới **local cluster**
```
# rbd --cluster remote mirror pool peer add vms client.admin@local
# rbd --cluster remote mirror pool peer add images client.admin@local
# rbd --cluster remote mirror pool peer add volumes client.admin@local
# rbd --cluster remote mirror pool peer add backups client.admin@local

# rbd --cluster remote mirror pool info donghm
```
**Note**: có thể xóa peer đã tạo cho các pool bằng lệnh `rbd mirror pool peer remove {pool-name} {peer-uuid}`


**Trên primary cluster**:  enable **journaling feature** cho các images trong các pools:
```
# rbd feature enable <pool_name>/<image_name> journaling
```

**Default config for rbd-mirror demon**
```
# ps aux | grep mirror
ceph     1392149  0.5  0.0 2948952 21168 ?       Ssl  10:31   0:00 /usr/bin/rbd-mirror

# ceph daemon /var/run/ceph/client.admin.1392149.remote.94643028643056.asok config show | grep mirror
    "bluestore_bluefs_env_mirror": "false",
    "debug_rbd_mirror": "0/5",
    "rbd_mirror_concurrent_image_deletions": "1",
    "rbd_mirror_concurrent_image_syncs": "5",
    "rbd_mirror_delete_retry_interval": "30.000000",
    "rbd_mirror_image_policy_migration_throttle": "300",
    "rbd_mirror_image_policy_rebalance_timeout": "0.000000",
    "rbd_mirror_image_policy_type": "none",
    "rbd_mirror_image_policy_update_throttle_interval": "1.000000",
    "rbd_mirror_image_state_check_interval": "30",
    "rbd_mirror_journal_commit_age": "5.000000",
    "rbd_mirror_journal_max_fetch_bytes": "32768",
    "rbd_mirror_journal_poll_age": "5.000000",
    "rbd_mirror_leader_heartbeat_interval": "5",
    "rbd_mirror_leader_max_acquire_attempts_before_break": "3",
    "rbd_mirror_leader_max_missed_heartbeats": "2",
    "rbd_mirror_pool_replayers_refresh_interval": "30",
    "rbd_mirror_sync_point_update_age": "30.000000",
    "rbd_mirroring_delete_delay": "0",
    "rbd_mirroring_replay_delay": "0",
    "rbd_mirroring_resync_after_disconnect": "false",
```


### Test delete image in primary

Trong trường hợp có người lỡ delete image trên primary thì image sẽ ngay lập tức bị xóa luôn mà k phải chuyển vào trash pool. Do đó có thể config **rbd_mirroring_delete_delay (seconds)** để khi xóa trên primary thì pool sẽ được chuyển vào trash pool và có thể restore lại kịp thời: 

Config **rbd_mirroring_delete_delay** trong **ceph.conf** trên **remote cluster**:
```
rbd_mirroring_delete_delay = 3600
```
Restart lại rbd mirror daemon và kiểm tra lại:
```
# systemctl restart ceph-rbd-mirror@admin
# ceph daemon /var/run/ceph/client.admin.1392149.remote.94643028643056.asok  config show | grep mirroring
```

Command để **restore** image trên **remote cluster**:
```
# rbd --pool vms trash list --all --format json --pretty-format
# rbd trash restore  image-id
```

### Tips

To increase performance, `image journal` can be created with: (also improves performance when writing to a journal for large request sizes.) [Link](https://github.com/ceph/ceph/blob/master/src/common/options.cc#L6734)
```
rbd_journal_max_payload_bytes = 1M
```

Config for `rbd-mirror`:
```
rbd_mirror_journal_max_fetch_bytes = 1M or more
```

`The rationale why we have so low defaults is to limit rbd-mirror memory usage when mirroring a pool with many images, and that a usual rbd workload is small size requests for which these params are not so useful.` [Link](http://tracker.ceph.com/issues/36652)

