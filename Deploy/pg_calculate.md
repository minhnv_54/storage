### Tính lại PGs cho từng Pool trên Ceph cluster tầng 5

#### 1. Thông tin về `capacity` của cluster:

| 		| Cụm hiện tại | Thêm | Cụm mới dự tính |
|-------------|-----------|-----------|-----------|
|**Số nodes** | 5 nodes 	| 3 nodes 	| 8 nodes 	|
| **Số OSDs** | 60 OSDs 	| 35 OSDs 	| **95 OSDs** 	|
| **Tổng dung lượng** | 144 TB 	| 140 TB 	| ***284 TB***	|

#### 2. Nhu cầu cần đáp ứng và mức độ sử dụng hiện tại 

 | Pool | PGs hiện tại | % storage để tính PGs hiện tại | % storage đã sử dụng (cho 125 VMs) |
 |:----------:|:------------:|:---------------:|:---------------:|
 | volumes-pool | 1024 | 40%  | 6% (= 2.4 TB) 	| 
 | backup-pool 	| 512  | 20%  | 10% (= 3.9 TB) 	|
 | images-pool 	| 128  | 5%   | 0.6% (= 259 GB)	 	|
 | metrics-pool | 128  | 5%   | 0.007% (= 3 GB) 	|
 | vms-pool 	| 256  | 10%  | 5% (= 2 TB) 		|
 |	| |**Capacity cũ: 40.5TB**  |

Capacity cũ chỉ là 40.5 TB do chưa tính số lượng pool size là 3.

**Đặc điểm một số Pool**:
- **metrics-pool**: sẽ tăng theo thời gian, thời gian bắt đầu sử dụng là 1/8 đến thời điểm tính lúc này là 19/11 = 110 ngày. Dự tính hoạt động trong 3 năm = 1095 ngày => sẽ cần 3Gb/110*1095 ~ **30 Gb**. 
- **backup-pool**: hiện tại đang là ~4TB sử dụng do a Đại chưa xóa bản backup của các ngày cũ ( ~10 backup cho mỗi volume). Thực tế với mỗi volume ước tính cần 3 bản backup => Dung lượng thực sự cần = % volumes-pool đã dùng * 3 = 2.4*3 = **7.2 TB**.
- **images-pool**: hiện tại đang lưu trữ 53 images, dung lượng lưu trữ cần cho pool này dự tính sẽ không quá 1TB.

Ngoài ra, hệ thống hiện tại cần cấp `167 VMs, đã cấp 125 VMs, chưa cấp 42 VMs `
=> Cần sử dụng thêm dung lượng trên 4 pool (trừ images-pool) bằng **1.35 %** dung lượng đã sử dụng hiện tại. Nghĩa là dung lượng storage sử dụng ước tính tại thời điểm khi cấp đủ **167 VMs** và sau **3 năm** sử dụng sẽ là:

 | Pool | PGs hiện tại | % storage để tính PGs hiện tại |  storage đã sử dụng|  storage sử dụng cho 167 VMs sau 3 năm | Dự tính % mới | Dự tính PGs mới |
 |:----------:|:------------:|:---------------:|:---------------:|:---------------:|:---------------:|:---------------:|
 | volumes-pool | 1024 | 40% | 2.4 TB (= 6%) | 2.4TB*1.35 = **3.24 TB** (4%)	| 15% | 1024 |
 | backup-pool 	| 512 | 20% | 3.9 TB (= 10%) | 3.24TB*3 = **9.72 TB** (12%)	| 40% | 2048 |
 | images-pool 	| 128 | 5% 	| 259 GB(= 0.6%) | **1 TB** (1.3%)	| 2% | 128 |
 | metrics-pool | 128 | 5% 	| 3 GB (= 0.007%) | 30GB*1.35 = **40.5 GB** (0.05%)	| 1% | 64 |
 | vms-pool 	| 256 | 10% | 2 TB (= 5%) | 2TB*1.35 = **2.7 TB** (3.4%) | 15% | 1024 |
 | | ~ 102 PGs / 1 OSD| **Capacity cũ: 40.5TB** | | **Capacity mới: 80.5TB** | 73% | **Tổng: 4288** (~135 PGs / 1 OSD) |
 
