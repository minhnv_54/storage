### Ceph RBD Mirroring

**Vấn đề**: khi 1 Ceph cluster bị 1 thảm họa nào đó: ví dụ như về mạng hoặc về điện

**Giải pháp của Ceph**: tạo 1 Ceph cluster khác để backup cho cluster hiện tại (thực hiện việc mirroring giữa 2 cluster) thay vì việc deploy thành 1 cụm duy nhất và tạo thêm nhiều replica cho pool vì:
- Ceph chỉ xem xét 1 hành động ghi là thành công khi dữ liệu đã được ghi trên tất cả các replica => trễ và gây ra block I/O
- 2 cụm có thể tách biệt về mặt địa lý: khác tầng, khác nguồn điện, mạng, thậm chí khác cả site.

**Ý tưởng**: dựa trên cơ chế **journaling** để update data giữa 2 cluster: sử dụng **rbd-mirror daemon**.

**Hiện tại**: mới chỉ support RBD mirroring từ phiên bản Luminous, có thể config RBD mirroring cho từng RBD pool hoặc cho từng image trong 1 pool.

**Yêu cầu**: Cần đảm bảo đủ dung lượng và hiệu năng trên secondary cluster cũng như băng thông kết nối giữa 2 cluster để giảm latency. Cụ thể: nếu ta có **X** MB/s là thông lượng ghi **trung bình** vào các images trên primary cluster thì network kết nối giữa 2 cluster phải hỗ trợ thông lượng là **N*X + Y%** (MB/s), trong đó N là số lượng images và Y% là *safety factor*. ([source](https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/3/html/block_device_guide/block_device_mirroring#rbd-mirroring-enable-journaling))

**Architecture**: 


![rbd_mirroring](https://i.imgur.com/XaxxTT5.png)

**rbd-mirror daemon**: là daemon chịu trách nhiệm thực hiện việc đồng bộ các images giữa 2 cluster. Tùy thuộc vào loại replica mà rbd-mirror sẽ chạy trên 1 cluster hoặc trên cả 2 cluster:
- **One-way replication**: hay còn gọi là `active-passive` configuration. Ở chế độ này, secondary cluster đóng vai trò là backup cluster cho primary cluster, và **rbd-mirror daemon** chỉ chạy trên backup cluster. (1 cluster có thể có nhiều backup cluster).
- **Two-way replication**: hay còn gọi là `active-active` configuration, 2 clusters sẽ cùng **mirror data** với nhau, do đó cần **rbd-mirror daemon** chạy trên cả 2 cluster. (Hiện tại mới chỉ support active-active giữa **2** cluster)

**Note**: `trên 1 cluster chỉ được chạy duy nhất 1 rbd-mirror daemon`.

RBD mirroring dựa trên 2 features của RBD image:
- **journaling**: enable journaling cho tất cả các transaction trên image. Cần enable feature **exclusive-lock** trước khi enable **journaling** ([source](http://docs.ceph.com/docs/master/man/8/rbd/?highlight=feature#cmdoption-rbd-image-feature)). 
- **mirroring**: image đã enable feature này khi có write operation sẽ request tới rbd-mirror daemon để thực hiện việc replicate image đó. Cần enable journaling trước khi enable mirroring. ([`mirror image enable image-spec`](http://docs.ceph.com/docs/master/man/8/rbd/?highlight=feature#commands))

**Mirroring modes**:
- **Pool mode**: khi config mirroring cho 1 pool thì tất cả images `đã enable journal` trong pool đó thi sẽ được enable mirror. Config tại [đây](https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/3/html/block_device_guide/block_device_mirroring#rbd-mirroring-configuring-pool-mirroring)
- **Image mode**: chỉ định những image nào cần mirror. See [Configuring Image Mirroring](https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/3/html/block_device_guide/block_device_mirroring#rbd-mirroring-configuring-image-mirroring "Configuring Image Mirroring") for details.

**Image states**: ở chế độ `active-passive` (one-way mirroring), *mirrored images* có 1 trong 2 trạng thái:
- **primary**: có thể modified (image trên primary cluster)
- **non-primary**: không thể modified. (image trên backup cluster)

Ta có thể thay đổi `image state` từ *primary* sang *non-primary* và ngược lại, commands tại [đây](https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/3/html/block_device_guide/block_device_mirroring#rbd-mirroring-image-promotion-demotion)

Tuy nhiên, khi config theo **one-way mirroring** thì cluster thứ 2 sẽ chỉ dùng được để backup, mà không thể thực hiện việc **automatic failover and faildback** khi primary cluster có lỗi, do các images trên secondary cluster không thể modified.



Quá trình mirroring:

![mirroring](http://www.sebastien-han.fr/images/ceph-rbd-mirror-inside.png)

Để đảm bảo về hiệu năng thì **journal-image** có thể nằm khác pool đối với các image mà ta cần mirroring.

Đối với các image đã tạo trước đó, thì về bản chất khi enable **journaling**, Ceph sẽ thực hiện việc tạo `snapshot` cho image đó, sau đó copy snapshot đó sang cluster kia.

**[Steps to config one-way mirroring](https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/3/html/block_device_guide/block_device_mirroring#configuring_one_way_mirroring)**

**[Steps to config two-way mirroring](https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/3/html/block_device_guide/block_device_mirroring#configuring_two_way_mirroring )**

