# Sử dụng FIO đánh giá hiệu năng của ceph cluster

**fio** là một công cụ mã nguồn mở dùng để đánh giá hiệu năng của các thiết bị lưu trữ

## Các bước thực hiện

Tạo pool

```
ceph osd pool create benchmark_pool 128 128
```

Tạo images

```
rbd create benchmark_image --size 10G --pool benchmark_pool --image-feature layering
```

Map image cho user

```
rbd map benchmark_image --pool benchmark_pool --name client.admin
```

Kiểm tra

```
[root@controller235 ~]# lsblk
NAME                          MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
sda                             8:0    0 278.5G  0 disk  
└─sda1                          8:1    0   200G  0 part  /
sdb                             8:16   0   500G  0 disk  
└─osd235_01                   253:1    0   500G  0 mpath 
  └─vg_osd235_01-lv_osd235_01 253:2    0   500G  0 lvm   
sdc                             8:32   0   500G  0 disk  
└─osd235_01                   253:1    0   500G  0 mpath 
  └─vg_osd235_01-lv_osd235_01 253:2    0   500G  0 lvm   
sdd                             8:48   0   500G  0 disk  
└─osd235_02                   253:0    0   500G  0 mpath 
  └─vg_osd235_02-lv_osd235_02 253:3    0   500G  0 lvm   
sde                             8:64   0   500G  0 disk  
└─osd235_02                   253:0    0   500G  0 mpath 
  └─vg_osd235_02-lv_osd235_02 253:3    0   500G  0 lvm   
rbd0                          252:16   0    10G  0 disk 
```

Format disk

```
mkfs.ext4 /dev/rbd0
```

Mount vào thư mục để sử dụng

```
mkdir /root/ceph_benchmark
mount /dev/rbd0 /root/ceph_benchmark
```

Tạo file kịch bản bench

```
[root@controller235 ~]# cat /root/ceph_benchmark/file_bench.fio 
[global]
ioengine=libaio
direct=1
[test_u02]
directory=/root/ceph_benchmark
filename=test
bs=4k
iodepth=64
size=5G
io_size=2G
readwrite=randrw
rwmixread=65
runtime=500
time_based
ramp_time=5
```

Giải thích các tham số:

**ioengine**: Xác định công cụ khởi tạo, cách thức thực thi I/O. Có một số tùy chọn:
 * sync: Khởi tạo và thực thi các I/O bởi các tiến trình đọc ghi đồng bộ thông
thường trên OS
 * libaio: Là công cụ khởi tạo và thực thi bất đồng bộ các I/O trên Linux
 * solarisaio: Là công cụ khởi tạo và thực thi bất đồng bộ các I/O trên Solaris
 * windowsaio: Là công cụ khởi tạo và thực thi bất đồng bộ các I/O trên
Windows
 * mmap: Là công cụ khởi tạo và thực thi các I/O bằng cách ánh xạ bộ nhớ trong,
I/O được ghi trực tiếp vào bộ nhớ đệm của kernel mà không phải thông qua
một system call như read hay write.

**I/O Type**: Xác định hành vi các I/O của workload được sử dụng cho các job. Các I/O chỉ đọc hoặc ghi, chỉ truy cập ngẫu nhiên hoặc tuần tự trong bộ nhớ hay kết hợp cả đọc và ghi, truy cập ngẫu nhiên và tuần tự. Các I/O đi qua cache của hệ điều hành, hay được đọc/ghi trực tiếp vào phân vùng lưu trữ.
Các tùy chọn cơ bản:
* direct:
  * true (1): I/O sẽ được đọc/ghi trực tiếp xuống thiết bị lưu trữ mà không qua bộ nhớ đệm từ kernel OS
  * false (0): I/O sẽ được đọc/ghi vào bộ nhớ đệm của kernel OS trước khi được chuyển xuống thiết bị lưu trữ
Lưu ý: OpenBSD và ZFS trên Solaris và các trình đồng bộ I/O trên Windows không hỗ trợ direct I/O. Giá trị mặc định là false.
* readwrite (rw) :
  * read : Đọc tuần tự
  * write: Ghi tuần tự
  * randread: Đọc ngẫu nhiên
  * randwrite: Ghi ngẫu nhiên
  * rw,readwrite: Kết hợp đọc ghi tuần tự
  * randrw : Kết hợp đọc, ghi ngẫu nhiên
* rwmixread: Tỉ lệ phần trăm I/O được đọc. Mặc định là 50% (Tỷ lệ đọc ghi này thường được đặt là 65/35)
* percentage_random: Tỉ lệ phần trăm I/O được đọc/ghi ngẫu nhiên

**blocksize(bs)**: Xác định kích thước của mỗi I/O được thực thi. giá trị mặc định là 4K cho cả I/O đọc và ghi, có thể điều chỉnh giá trị cho I/O đọc, ghi riêng biệt. Ví dụ: bs= 4K, 8K , điều này có nghĩa là các I/O đọc sẽ có độ lớn 4K và ghi sẽ là 8K.

**I/O size**: Xác định kích thước dữ liệu tải được giả lập cho quá trình test.
Các tùy chọn cơ bản:
* size: Chỉ định tổng kích thước của các file I/O cho mỗi luồng thực thi. FIO sẽ thực thi cho đến khi toàn bộ các I/O này được đọc/ghi, trừ khi có những giới hạn bởi các tham số khác (ví dụ giới hạn về thời gian chạy với tùy chọn runtime hay việc tăng, giảm kích thước các I/O được thực thi bởi tùy chọn io_sizie )
* io_size: Thông thường, FIO sẽ thực thi trong vùng nhớ có kích thước được thiết lập bởi tùy chọn size. Điều này có nghĩa tùy chọn size sẽ quyết định cả kích thước của vùng nhớ và tổng các I/O được thực thi. Tùy chọn io_size cho phép thay kích thước tổng của các I/O. Ví dụ, Khi size=40GB, io_size=20GB FIO sẽ dừng lại khi thực thi xong tổng kích thước I/O là 20GB và việc thực thi này được thực hiện trong một vùng nhớ 40GB.

**Target file/device**: Xác định files, phân vùng được chỉ định cho việc đọc/ghi các I/O của workload. Tham số này cho phép thiết đặt vùng đổ tải test là file, hay một phân vùng nhất định.
Các tùy chọn cơ bản:
* directory: Chỉ định thư mục chứa file được tạo phục vụ test đổ tải. VD:/tmp/FIO/
* filename: Tên file hoặc phân vùng được chỉ định

**Time related parameters**: Xác định các thiết lập về thời gian như thời gian chạy test, thời gian lấy mẫu dữ liệu báo cáo.
Các tùy chọn cơ bản:
* runtime: Chỉ định khoảng thời gian để FIO thực hiện thực thi một job hoặc job file.
* time_based: Chỉ định thời gian chạy theo tham số runtime. Điều này có nghĩa, trong thời gian runtime, khi việc đổ tải hoàn tất trên file được chỉ định, FIO sẽ thực hiện lại việc đổ tải.
* ramp_time: Chỉ định khoảng thời gian từ lúc bắt đầu thực thi các job, job files cho đến khi FIO bắt đầu ghi lại thông tin đầu ra. Khuyến nghị thiết đặt giá trị từ 5s đến 10s để bỏ qua thời gian khởi tạo ban đầu (thường không ổn định) trước khi lấy mẫu dữ liệu đánh giá.
**Threads, processes and job synchronization**: Xác định số luồng hay tiến trình được sử dụng cho workload 

## Kết quả bench

```
[root@controller235 bench_ceph1]# fio file_bench.fio
test_u02: (g=0): rw=randrw, bs=(R) 4096B-4096B, (W) 4096B-4096B, (T) 4096B-4096B, ioengine=libaio, iodepth=64
fio-3.7
Starting 1 process
test_u02: Laying out IO file (1 file / 5120MiB)
Jobs: 1 (f=1): [m(1)][100.0%][r=30.3MiB/s,w=15.9MiB/s][r=7748,w=4080 IOPS][eta 00m:00s] 
test_u02: (groupid=0, jobs=1): err= 0: pid=235875: Tue Nov 10 16:00:34 2020
   read: IOPS=11.5k, BW=44.0MiB/s (47.1MB/s)(21.0GiB/500005msec)
    slat (usec): min=4, max=9591, avg=21.44, stdev=24.37
    clat (usec): min=2, max=872753, avg=2496.10, stdev=8958.83
     lat (usec): min=173, max=872766, avg=2517.89, stdev=8959.87
    clat percentiles (usec):
     |  1.00th=[   318],  5.00th=[   457], 10.00th=[   562], 20.00th=[   734],
     | 30.00th=[   914], 40.00th=[  1123], 50.00th=[  1369], 60.00th=[  1696],
     | 70.00th=[  2147], 80.00th=[  2802], 90.00th=[  3654], 95.00th=[  4752],
     | 99.00th=[ 20055], 99.50th=[ 41157], 99.90th=[127402], 99.95th=[170918],
     | 99.99th=[325059]
   bw (  KiB/s): min=    8, max=75560, per=99.89%, avg=45985.98, stdev=18067.48, samples=1000
   iops        : min=    2, max=18890, avg=11496.29, stdev=4516.83, samples=1000
  write: IOPS=6198, BW=24.2MiB/s (25.4MB/s)(11.8GiB/500005msec)
    slat (usec): min=5, max=43292, avg=27.32, stdev=41.32
    clat (usec): min=985, max=880402, avg=5615.61, stdev=12135.30
     lat (usec): min=1148, max=880410, avg=5643.32, stdev=12135.49
    clat percentiles (usec):
     |  1.00th=[  1713],  5.00th=[  2147], 10.00th=[  2474], 20.00th=[  2900],
     | 30.00th=[  3261], 40.00th=[  3621], 50.00th=[  4015], 60.00th=[  4490],
     | 70.00th=[  5014], 80.00th=[  5669], 90.00th=[  7111], 95.00th=[ 10028],
     | 99.00th=[ 38011], 99.50th=[ 68682], 99.90th=[168821], 99.95th=[221250],
     | 99.99th=[438305]
   bw (  KiB/s): min=   24, max=40814, per=99.89%, avg=24764.98, stdev=9739.08, samples=1000
   iops        : min=    6, max=10201, avg=6191.05, stdev=2434.74, samples=1000
  lat (usec)   : 4=0.01%, 20=0.01%, 50=0.01%, 100=0.01%, 250=0.13%
  lat (usec)   : 500=4.34%, 750=9.24%, 1000=8.75%
  lat (msec)   : 2=22.42%, 4=32.61%, 10=19.42%, 20=1.67%, 50=0.90%
  lat (msec)   : 100=0.32%, 250=0.18%, 500=0.02%, 750=0.01%, 1000=0.01%
  cpu          : usr=9.79%, sys=26.32%, ctx=4021219, majf=0, minf=1487
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=0.1%, 32=0.1%, >=64=100.1%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.1%, >=64=0.0%
     issued rwts: total=5754880,3099221,0,0 short=0,0,0,0 dropped=0,0,0,0
     latency   : target=0, window=0, percentile=100.00%, depth=64

Run status group 0 (all jobs):
   READ: bw=44.0MiB/s (47.1MB/s), 44.0MiB/s-44.0MiB/s (47.1MB/s-47.1MB/s), io=21.0GiB (23.6GB), run=500005-500005msec
  WRITE: bw=24.2MiB/s (25.4MB/s), 24.2MiB/s-24.2MiB/s (25.4MB/s-25.4MB/s), io=11.8GiB (12.7GB), run=500005-500005msec

Disk stats (read/write):
  rbd1: ios=5760115/3102257, merge=0/139, ticks=11530353/15646779, in_queue=26678659, util=100.00%
```

Như kết quả trên ta thấy hiệu năng đọc vào khoảng IOPS=11.5k, BW=44.0MiB/s, latency=4.7ms và hiệu băng ghi vào khoảng IOPS=6198, BW=24.2MiB/s, latency=10ms
