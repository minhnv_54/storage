## Placement groups (PGs)

Khi Ceph cluster nhận yêu cầu ghi dữ liệu từ client, nó sẽ chia dữ liệu thành các object ( kích thước mặc định 4MiB/object) và được gán object này cho 1 Placement Group (PG). Mỗi PG sẽ được map tới 1 nhóm OSD số lượng OSD trên 1 PG tương ứng với số lượng replica của mỗi pool (1 OSD được nhiều PG map tới).

![image](/uploads/38f633a3335d21aba2cb2fb7b6040db4/image.png)

Như vậy trên 1 PG sẽ có 1 primary OSD và các OSD còn lại. Khi object được đẩy xuống PG nó sẽ lưu vào primary OSD trước và sau đó sẽ được copy sang các OSD còn lại để đảm bảo số lượng replica.

PGs được thiết kế đáp ứng khả năng mở rộng, hiệu suất cao trong Ceph storage system, đồng thời hỗ trợ việc quản trị Object. Nếu không có các PGs, việc quản trị dữ liệu sẽ trở nên rất khó, cùng với đó là khả tổ chức các object đã được nhân bản (hàng triệu object) tới hàng trăm các OSD khác nhau. Qua đó, thay vì quản trị object riêng biệt, hệ thông sẽ sử dụng PGs. PGs sẽ khiến ceph dễ quản trị dữ liệu và giảm bớt sự phức tạp trong khâu quản lý.

Số lượng PGs trong cluster cần được tính toán. Thông thường, tăng số lượng PGs trong cluster sẽ giảm bớt gánh nặng trên mỗi OSD, nhưng cần xem xét theo quy chuẩn. Khuyến nghị 100-200 PGs trên mỗi OSD.  Khi thiếtt bị mới được thêm, xóa bỏ khói cluster, các PGs sẽ vẫn tồn tại – CRUSH sẽ quản lý việc tài cấp phát PGs trên toàn cluster.

### Công thức tính số PG

```
Total PGs = (Total_number_of_OSD * 100) / max_replication_count

Kết quả có thể làm tròn gần nhất theo 2 ^ đơn vị.
```

Có thể áp dụng [tool](https://ceph.io/pgcalc/) để tính số PG cho từng pool

## Trạng thái của PG

| Trạng thái	| Thông tin chi tiết |
| --------- | ---------------------- |
| creating | PG đang trong trạng thái khởi tạo |
| activating | PG khởi tạo xong đang trong trạng thái active hoăc chờ sử dụng |
| active | PG active sẵn sàng xử lý các request của client đọc ghi cập nhật ... |
| clean | Các bản ghi replicate đúng vị trí và đủ số lượng |
| down | Một bản sao có thiết bị hỏng và PG không sẵn sàng xử lý các request |
| scrubbing | Ceph kiểm tra metadata của PG không nhất quán giữa các bản replica |
| deep | Ceph kiểm tra data trên các PG bằng cách kiểm tra checksums data |
| degraded | Chưa replica đủ số bản ghi (x2 x3) theo cấu hình do có 1 OSD trong PG bị lỗi hoặc treo |
| inconsistent | Kiểm tra sự không nhất quán của dữ liệu (sai vị trí thiếu bản ghi hoặc bản ghi không nhất quán) |
| peering |	PG đang trong quá trình chuẩn bị xử lý |
| repair | Kiểm tra và sửa các PG có trạng thái inconsistent nếu nó thấy |
| recovering | Hệ thống đang di chuyển đồng bộ các object đúng đủ bản sao của nó |
| forced_recovery |	Ưu tiên phục hồi PG bởi người dùng |
| recovery_wait | PG đang trong hàng đợi chờ để recover |
| recovery_toofull | PG đang chờ để recover nhưng đã vượt qua ngưỡng lưu trữ |
| backfilling |	Ceph đang quét và đồng bộ hóa toàn bộ nội dung của một PG thay vì suy ra nội dung nào cần được đồng bộ hóa từ nhật ký của các hoạt động gần đây. backfilling là một trường hợp đặc biệt của recover. |
| forced_backfill |	Ưu tiên backfill PG bởi người dùng |
| backfill_wait | PG đang trong hàng đợi chờ backfill |
| backfill_toofull | PG đang trong hàng đợi chờ backfill nhưng đã vượt quá dung lượng lưu trữ của cụm |
| backfill_unfound | PG dừng quá trình backfill vì không đối chiếu kiểm tra được dữ liệu của PG |
| incomplete | Hệ thống kiểm tra thấy PG thiếu thông tin về dữ liệu của nó hoặc không có bản sao nào chắc chắn đúng. Cần thử start lại OSD đã hỏng nào có data đúng hoặc giảm min_size để cho phép khôi phục |
| stale | Trạng thái không xác định có thể chưa nhận được cập nhật từ lần thay đổi pg_map lần cuối cùng |
| remapped | PG được lưu trữ ánh xạ qua 1 vị trí trên OSD khác theo sự chỉ định của CRUSH |
| undersized | Có ít bản sao hơn so với cấu hình của pool |
| peered | Đã chuẩn bị xử lý nhưng chưa phục vụ các thao tác IO của client do ko đủ bản sao theo min_size |
| snaptrim | Dọn dẹp bản snapshot |
| snaptrim_wait | PG vào hàng đợi chờ dọn dẹp snapshot |
| snaptrim_error | Lỗi trong quá trình dọn dẹp snapshot |

Tham khảo: https://programmer.group/detailed-explanation-of-pg-state-of-distributed-storage-ceph.html
