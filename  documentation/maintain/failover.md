## Failover

### 1. Import keyring for user nova, cinder, cinder-backup, glance for backup cluster

**Thực hiện trên node mon của remote cluster:** copy các file `keyring` của user `nova, cinder, cinder-backup, glance` từ `local cluster` sang `remote cluster` và chỉnh sửa theo nội dung bên dưới, sau đó `import`:

```
# cat /etc/ceph/ceph.client.nova.keyring
[client.nova]
	key = AQCdjghcG0InNRAAg2+7FknxaQ2vbOg2Lz0Jmw==
	
	caps mon = "profile rbd"
	caps osd = "profile rbd pool=volumes, profile rbd pool=vms, profile rbd pool=images"

# ceph auth import -i ceph.client.nova.keyring
```

```
# cat ceph.client.glance.keyring
[client.glance]
	key = AQAliAhceHGvHBAAaFMwQujL1o1nB5vYeIc0/A==

	caps mon = "profile rbd"
	caps osd = "profile rbd pool=images"

# ceph auth import -i ceph.client.glance.keyring
```

```
# cat ceph.client.cinder.keyring 
[client.cinder]
	key = AQA0iAhcfyAGFxAAtW6YVPp8o8Pr8SlFbHDxvg==

	caps mon = "profile rbd"
	caps osd = "profile rbd pool=volumes, profile rbd pool=vms, profile rbd pool=images"

# ceph auth import -i ceph.client.cinder.keyring
```
```
# cat ceph.client.cinder-backup.keyring 
[client.cinder-backup]
	key = AQA7iAhcrvXBDBAALmfYiGFJgU6G1q16+fyNsA==

	caps mon = "profile rbd"
	caps osd = "profile rbd pool=backups"

# ceph auth import -i ceph.client.cinder-backup.keyring
```

Kiểm tra lại:

```
# ceph auth list
```

### 2.  Promote - Demote các pool trên backup cluster và primary cluster
**Demote** các pool cần thiết trên `primary cluster`:
```
# rbd mirror pool demote vms
# rbd mirror pool demote images
# rbd mirror pool demote volumes
# rbd mirror pool demote backups
```

**Promote** các pool cần thiết trên `backup cluster`:
```
# rbd mirror pool promote --force vms
# rbd mirror pool promote --force images
# rbd mirror pool promote --force volumes
# rbd mirror pool promote --force backups
```

### 3. Chạy lại Kolla-ansible reconfig

Copy file `ceph.conf` của `backup cluster` tới các thư mục 
- /etc/kolla/config/cinder/ceph.conf
- /etc/kolla/config/nova/ceph.conf
- /etc/kolla/config/glance/ceph.conf

Chạy `kolla-ansible -t nova,glance,cinder reconfigure`
