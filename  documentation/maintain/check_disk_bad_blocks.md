## Check badblocks
```
badblocks -svn /dev/sdx -o badblocks.log
```
With:
- s: show progress
- v: verbose
- n: Use  non-destructive  read-write mode.  By default only a non-destructive read-only test is done.
