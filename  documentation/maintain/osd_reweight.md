# Reweight

### Dùng thủ công

```
ceph osd reweight {osd-num} {weight}
```

VD

```
ceph osd reweight 10 0.95
```

### Dùng tool

```
ceph osd test-reweight-by-utilization [threshold [max_change max_osds]]
ceph osd reweight-by-utilization [threshold [max_change [max_osds]]]
```

VD

```
ceph osd test-reweight-by-utilization 120 0.05 3
ceph osd reweight-by-utilization 120 0.05 3
```
