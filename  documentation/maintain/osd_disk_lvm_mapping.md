# OSDs <-> Physical Disks

In case, we enabled mgr dashboard:

- We can get ID of OSD from dashboard
- Then use `lsblk` command in OSD nodes to see the mapping between **OSD id, Volume group ID and physical volume (physical disk or partion)**

The result of `lsblk` command will show in a format like this:

```
<PV Name>
└─<VG Name>-osd--block--<OSD ID> 253:6    0  3,7T  0 lvm
```

For instance:

```
[root@ceph-osd1 ceph-22]# lsblk
NAME                                                                                                  MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda                                                                                                     8:0    0  3,7T  0 disk
├─sda1                                                                                                  8:1    0  200M  0 part /boot/efi
├─sda2                                                                                                  8:2    0    1G  0 part /boot
├─sda3                                                                                                  8:3    0    4G  0 part [SWAP]
└─sda4                                                                                                  8:4    0  3,6T  0 part /
sdb                                                                                                     8:16   0  3,7T  0 disk
└─ceph--02f4b26d--ce6d--467b--8b76--b635b14f3364-osd--block--2bb59270--e634--46c7--8a27--c6613d54ed9f 253:1    0  3,7T  0 lvm
sdc                                                                                                     8:32   0  3,7T  0 disk
└─ceph--73097b96--d2d1--4fca--b8c3--bb5b7776a590-osd--block--df40707d--6e1e--474d--9b3b--9dfe52433311 253:0    0  3,7T  0 lvm
sdd                                                                                                     8:48   0  3,7T  0 disk
└─ceph--6062fdc0--6433--4019--bb39--8e4de034c316-osd--block--0b65362d--b484--43c3--94d5--fa9300faf22e 253:3    0  3,7T  0 lvm
sde                                                                                                     8:64   0  3,7T  0 disk
└─ceph--70cd175b--c467--4e10--ad77--8d7cf8015be4-osd--block--04f302b8--eec7--4192--8208--78d8f281961c 253:2    0  3,7T  0 lvm
sdf                                                                                                     8:80   0  3,7T  0 disk
└─ceph--54d81f8f--9e3d--4294--bec0--89774dc98cb0-osd--block--e226ac91--03c2--414e--946d--6b6a13f3dcc2 253:4    0  3,7T  0 lvm
sdg                                                                                                     8:96   0  3,7T  0 disk
└─ceph--3defbd1a--f25f--4b42--9742--9e7ca9ff9420-osd--block--02634a06--e4e6--4a80--a686--74126a587258 253:5    0  3,7T  0 lvm
sdh                                                                                                     8:112  0  3,7T  0 disk
└─ceph--31db4098--44cb--40f0--ac3c--2df7a11b0140-osd--block--d1ab46fd--d5cc--4fc0--9bc7--7bae95dccd7b 253:6    0  3,7T  0 lvm

```


# Related commands

- Deploy node:/cluster: `ceph-deploy osd list <osd_host>` **OSd <-> Volume group <-> OSD ID**

```
[ceph-osd2][DEBUG ] ====== osd.8 =======
[ceph-osd2][DEBUG ]
[ceph-osd2][DEBUG ]   [block]    /dev/ceph-bd899b35-37b5-41d6-b7e7-7463affe059a/osd-block-affdab9a-46c5-45a4-8382-f30fe5a783b4
[ceph-osd2][DEBUG ]
[ceph-osd2][DEBUG ]       type                      block
[ceph-osd2][DEBUG ]       osd id                    8
[ceph-osd2][DEBUG ]       cluster fsid              73097b96-d2d1-4fca-b8c3-bb5b7776a590
[ceph-osd2][DEBUG ]       cluster name              ceph
[ceph-osd2][DEBUG ]       osd fsid                  affdab9a-46c5-45a4-8382-f30fe5a783b4
[ceph-osd2][DEBUG ]       block uuid                0cv7zf-P155-cbwI-d9i5-2d8v-NfGS-8Q4VRY
[ceph-osd2][DEBUG ]       block device              /dev/ceph-bd899b35-37b5-41d6-b7e7-7463affe059a/osd-block-affdab9a-46c5-45a4-8382-f30fe5a783b4


```
- OSD nodes:
    - `sudo pvdisplay`: **Disk <-> Volume group**
    ```
      --- Physical volume ---
        PV Name               /dev/sdh
        VG Name               ceph-31db4098-44cb-40f0-ac3c-2df7a11b0140
        PV Size               <3,64 TiB / not usable <3,84 MiB
        Allocatable           yes (but full)
        PE Size               4,00 MiB
        Total PE              953861
        Free PE               0
        Allocated PE          953861
        PV UUID               u0DSqL-27aC-Vt32-3mKT-udwj-1l5a-CVGXFX
    ```
	- `sudo pvscan`: **Disk <-> Volume group**
    ```
        PV /dev/sde   VG ceph-70cd175b-c467-4e10-ad77-8d7cf8015be4   lvm2 [<3,64 TiB / 0    free]
        PV /dev/sdg   VG ceph-3defbd1a-f25f-4b42-9742-9e7ca9ff9420   lvm2 [<3,64 TiB / 0    free]
        PV /dev/sdf   VG ceph-54d81f8f-9e3d-4294-bec0-89774dc98cb0   lvm2 [<3,64 TiB / 0    free]
        PV /dev/sdc   VG ceph-73097b96-d2d1-4fca-b8c3-bb5b7776a590   lvm2 [<3,64 TiB / 0    free]
        PV /dev/sdb   VG ceph-02f4b26d-ce6d-467b-8b76-b635b14f3364   lvm2 [<3,64 TiB / 0    free]
        PV /dev/sdd   VG ceph-6062fdc0-6433-4019-bb39-8e4de034c316   lvm2 [<3,64 TiB / 0    free]
        PV /dev/sdh   VG ceph-31db4098-44cb-40f0-ac3c-2df7a11b0140   lvm2 [<3,64 TiB / 0    free]
        Total: 7 [25,47 TiB] / in use: 7 [25,47 TiB] / in no VG: 0 [0   ]
    ```
