### Ceph

**Ceph philosophy:**
- Every components must be scalable
- No individual process or server or other component can be a SPOF
- The solution must be software-base, open source and adaptable
- Ceph software should run on readily available commodity hardware without vendor lock-in.
- Everything must be self-managing if possible.

**Ceph history:**

- Developed by **Sage Weil** in 2003 as a part of hist PhD project, provided the CephFS.
- Opened source in 2006
- In 2012 **Sage Weil** founded **Inktank** to enable the widespread adoption of Ceph.
- In 2014 Red Hat agreed to acquire Inktank.

Through the **Luminous** release (08 - 2017) the Ceph community tagged a new major version about twice a year.

Starting with **Mimic** (2018) there will no longer be alternating LTS and stable releases. Each release henceforth will be LTS at a roughly 9 month cadence. Detail at [here](https://github.com/ceph/ceph/pull/18117/files)

### Ceph daemons and components

![ceph_architecture](http://docs.ceph.com/docs/mimic/_images/ditaa-804f45fe5a789fa161d7b4100740adf992a0dc07.png)

- **RADOS**: is foundation of Ceph at low-level data store. RADOS is an object storage system layer that provides: a data durability and availability framework.
	- Highly available with no SPOF
	- Reliable, scalable and adaptive
	- Self-healing and self-managing
- **Ceph Managers (ceph-mgr)**:
	- Keeping track of: runtime metrics, current state of Ceph cluster 
	- Host python-base plugins to manage and export Ceph cluster info (include web-based **Ceph Manager Dashboard** and **REST API**)
- **Ceph Monitor (ceph-mon)**
	- Maintain **maps** of Ceph cluster state: monitor map, manager map, OSD map, CRUSH map and PG map.
	- Manage authentication between daemons and clients (using cephX).
- **Ceph  Object Storage Daemon (ceph-osd)**: 
	- Stores data
	- Handles data replication, recovery, rebalancing (by using **CRUSH** algorithm)
	- Provide monitor information for Ceph Monitor and Managers by checking other Ceph OSD Daemons for a [heartbeat](http://docs.ceph.com/docs/giant/rados/configuration/mon-osd-interaction/)
- **Ceph Metadata Server (ceph-mds)**:
	- Stores metadata on behalf of the Ceph Filesystem
- **RADOS GateWay (RGW)**
	- Offer a highly scalable API-style interface to data organized, compatible with S3 and Swift.

### Cluster Map

**1. Monitor Map**: contains the **cluster fsid**, position, name address, port of each monitor and epoch info.
```sh
[root@osd30 ~]# ceph mon dump
dumped monmap epoch 3
epoch 3
fsid 649eb483-f390-402c-9695-543bba0e5df7
last_changed 2018-07-25 14:25:11.548982
created 2018-07-25 11:27:40.205730
0: 10.240.201.11:6789/0 mon.controller11
1: 10.240.201.13:6789/0 mon.controller13
2: 10.240.201.14:6789/0 mon.controller14
```

**2. OSD Map**: contain the cluster fsid, when the map was created and last modified, a list of pools, replica sizes, PG numbers, a list of OSDs and their status (e.g., `up`, `in`).

The Ceph OSD Daemon status is `up` or `down` reflecting whether or not it is running and able to service Ceph Client requests. If a Ceph OSD Daemon is `down` and `in` the Ceph Storage Cluster, this status may indicate the failure of the Ceph OSD Daemon.

```sh
[root@osd30 ~]# ceph osd dump
epoch 10808
fsid 649eb483-f390-402c-9695-543bba0e5df7
created 2018-07-25 11:27:41.043727
modified 2018-11-08 14:11:16.681433
flags sortbitwise,recovery_deletes,purged_snapdirs
crush_version 248
full_ratio 0.95
backfillfull_ratio 0.9
nearfull_ratio 0.85
require_min_compat_client jewel
min_compat_client jewelrados
require_osd_release mimic
pool 13 'backups' replicated size 3 min_size 2 crush_rule 0 object_hash rjenkins pg_num 512 pgp_num 512 last_change 10727 flags hashpspool,selfmanaged_snaps stripe_width 0 application rbd
	removed_snaps [1~5d,5f~42,a2~48]
```

**3. PG Map**: Contains the PG version, its time stamp, the last OSD map epoch, the full ratios, and details on each placement group such as the PG ID, the Up Set, the Acting Set, the state of the PG (e.g., `active  +  clean`), and data usage statistics for each pool.

**4. CRUSH Map**: Contains a list of storage devices, the failure domain hierarchy (e.g., device, host, rack, row, room, etc.), and rules for traversing the hierarchy when storing data
```sh
$ ceph  osd  getcrushmap  -o  {filename}
$ crushtool  -d  {comp-crushmap-filename}  -o  {decomp-crushmap-filename}
```
**5. MDS Map**: Contains the current MDS map epoch, when the map was created, and the last time it changed. It also contains the pool for storing metadata, a list of metadata servers, and which metadata servers are `up` and `in`.
```sh
[root@osd30 ~]# ceph fs dump
dumped fsmap epoch 1
e1
enable_multiple, ever_enabled_multiple: 0,0
compat: compat={},rocompat={},incompat={1=base v0.20,2=client writeable ranges,3=default file layouts on dirs,4=dir inode in separate object,5=mds uses versioned encoding,6=dirfrag is stored in omap,8=no anchor table,9=file layout v2,10=snaprealm v2}
legacy client fscid: -1
 
No filesystems configured
```

### Ceph Authentication

Ceph provides its **cephx** authentication system to authenticate users and daemons. Cephx uses shared secret keys for authentication, meaning both the client and the monitor cluster have **a copy** of the client’s **secret key**.

Ceph presets the  `keyring`  setting with the following four keyring names by default so you don’t have to set them in your Ceph configuration file unless you want to override the defaults (not recommended):

-   `/etc/ceph/$cluster.$name.keyring`
-   `/etc/ceph/$cluster.keyring`
-   `/etc/ceph/keyring`
-   `/etc/ceph/keyring.bin`

**Note**: Ceph stories keys in plaintext files

**Create user follow**

![create_client_user](http://docs.ceph.com/docs/mimic/_images/ditaa-6b1dafb6d8f177ab2beb3325857f1e98e4593ec6.png)

**Note**: `If you provide a user with capabilities to OSDs, but you DO NOT restrict access to particular pools, tThe user will have access to ALL pools in the cluster!`

**Detail requests to user access an Object**

![access_object](http://docs.ceph.com/docs/mimic/_images/ditaa-f97566f2e17ba6de07951872d259d25ae061027f.png)

*(Source: http://docs.ceph.com/docs/mimic/architecture/#high-availability-authentication)*

```sh
$ ceph auth ls
osd.9
	key: AQB+Uo9bODvmEBAAQRn+arH7qPetTxN/+QdkUg==
	caps: [mgr] allow profile osd
	caps: [mon] allow profile osd
	caps: [osd] allow *
client.admin
	key: AQA9/FdbORKlAhAAp+r9LKl913uxpcrELdcTIA==
	caps: [mds] allow *
	caps: [mgr] allow *
	caps: [mon] allow *
	caps: [osd] allow *
client.bootstrap-mds
	key: AQA9/Fdbo0SlAhAAqGrrjwwTG8wWFWjrCUj3jQ==
	caps: [mon] allow profile bootstrap-mds
client.bootstrap-mgr
	key: AQA9/FdbBGalAhAA6EUpJDygSobFe99/CHencA==
	caps: [mon] allow profile bootstrap-mgr
client.bootstrap-osd
	key: AQA9/FdbEImlAhAAqHckKBpmVGJ3j//1YqdguQ==
	caps: [mon] allow profile bootstrap-osd
client.bootstrap-rbd
	key: AQA9/Fdb+bClAhAAEtKHIi+fdfD6iQq6Ds7rvg==
	caps: [mon] allow profile bootstrap-rbd
client.bootstrap-rgw
	key: AQA9/Fdbkc6lAhAAOfm+jjPP2skkDoFyP7pqTQ==
	caps: [mon] allow profile bootstrap-rgw
client.cinder
	key: AQArMlhbKNSQBhAA7RZYs4gJDNnc+YYDt7NxqA==
	caps: [mon] allow r
	caps: [osd] allow class-read object_prefix rbd_children, allow rwx pool=volumes, allow rwx pool=vms, allow rx pool=images
client.cinder-backup
	key: AQArMlhbqRcDMRAAKlN7s5LZN1l9GngdM0MM/A==
	caps: [mon] allow r
	caps: [osd] allow class-read object_prefix rbd_children, allow rwx pool=backups
client.glance
	key: AQApMlhb6Kk3JhAAOUAh2izU1RSTudsoJpANEw==
	caps: [mon] allow r
	caps: [osd] allow class-read object_prefix rbd_children, allow rwx pool=images
client.gnocchi
	key: AQAtMlhbZBStJBAAwrhqFtBLJuE6hmFj7jhWTQ==
	caps: [mon] allow r
	caps: [osd] allow class-read object_prefix rbd_children, allow rwx pool=metrics
client.nova
	key: AQAqMlhbB3SXGRAAUT9/7ZSTBmJRlG+i03VDxg==
	caps: [mon] allow r
	caps: [osd] allow class-read object_prefix rbd_children, allow rwx pool=volumes, allow rwx pool=vms, allow rwx pool=images
client.rgw.monitor49
	key: AQCA0YRbS/WTIxAAaxf6rvvl31WPNJyt+t3UMw==
	caps: [mon] allow rw
	caps: [osd] allow rwx
client.rgw.monitor50
	key: AQB60YRbeOYLJxAAlgnDr0kufiwcoTrFD6hGwg==
	caps: [mon] allow rw
	caps: [osd] allow rwx
client.rgw.monitor74
	key: AQD8z4RbNlSeGBAAppuge3+qCNMwXAmuKSof/g==
	caps: [mon] allow rw
	caps: [osd] allow rwx
mgr.controller11
	key: AQBt/FdbnhYzDxAAb5psSZf5MPUjoTg9GBcc5w==
	caps: [mds] allow *
	caps: [mon] allow profile mgr
	caps: [osd] allow *
mgr.controller13
	key: AQBu/FdbCT/uLxAAXsh256jr+OIKfeI83sbz0w==
	caps: [mds] allow *
	caps: [mon] allow profile mgr
	caps: [osd] allow *
```
For more command [here](http://docs.ceph.com/docs/mimic/rados/operations/user-management/)

### Ceph Pools
When the first deploy a cluster without creating a pool, Ceph uses the default pools for storing data. A pool provides:
- Durability: **replica** or **erasure code**
- Placement group
- CRUSH Rules
- Snapshots

### Ceph placement group
**PG** is a logical collection of objects that are replicated on OSDs (the number of replication is defined at the pool that **PG** is located)
- PGs are essential for the **scalability** and **performance** of Ceph cluster. Without PGs, it will be difficult to **manage and track** tens of millions of **objects** that are **replicated** and spread over hundreds of **OSDs**.
- Instead of managing every object individually, a system has to manage the PGs with **numerous objects**.
- Placement groups do not own the OSD, they share it with other placement groups from the same pool or even other pools
- Each PG requires some **system resource** for **manage multiple objects**. Increasing the PG Count of a pool is one of the most impactful events in a Ceph Cluster, and should be avoided for production clusters if possible. **=>** `The number of PGs in a cluster should be meticulously calculated`.
- **States**:
	- **Splitting:** The PG is being split into multiple PGs. For example, if you increase the PGs of a pool rbd from 64 to 128, the existing PGs will split, and some of their objects will be moved to new PGs.
	- **Down:** A replica with necessary data is down, so the PG is offline (down).
	- **Recovering:** Objects are being migrated/synchronized with replicas. When an **OSD** goes down, its contents may fall behind the current state of other replicas in the PGs. So, the PG goes into a recovering state and objects will be migrated/synchronized with replicas.
	- **Inconsistent:** The PG replica is not consistent. For example, there is the wrong size of object, or objects are missing from one replica after recovery is finished.
	- **Incomplete:** A PG is missing a necessary period of history from its log. This generally occurs when an OSD that contains needed information fails or is unavailable.vices presented to users
	- **Undersized**: The placement group has fewer copies than the configured pool replication level.
	- **Degraded**: Ceph has not replicated some objects in the placement group the correct number of times yet.

**Note**: Troubleshooting PG at [here](http://docs.ceph.com/docs/mimic/rados/troubleshooting/troubleshooting-pg/#)

**Cách tính PGs cho 1 pool**
- Các thông số cần biết:
	- **Số lượng OSD**
	- **Size**: số **replica** của pool
	- **% data**: phần trăm sử dụng các OSDs của pool này (do 1 OSD có thể được gán cho nhiều pool để lưu trữ dữ liệu)
	- **Target PGs per OSD**: dựa trên dự định **scale** hê thống (tăng thêm số lượng **OSD**)
		- **100** nếu dự tính **không tăng** OSD
		- **200** nếu dự tính **tăng** (gấp đôi) số lượng OSD
- **Công thức tính**:
	- **Bước 1**
		```sh
		( Target PGs per OSD ) * ( OSD # ) * ( %Data )		( OSD # )
		----------------------------------------------- >< -----------
				( Size )									( Size )
		```
	- **Bước 2**: tìm lũy thừa của 2 gần nhất với giá trị tính được trong bước 1
		```sh
		function nearestPow2( aSize ){
		  return Math.pow( 2, Math.round( Math.log( aSize ) / Math.log( 2 ) ) ); 
		}
		```
	- **Bước 3**: nếu giá trị tìm được trong bước 2 lệch hơn so với giá trị tính được trong bước 1 **25%** thì lấy số lũy thừa tiếp theo của 2 cao hơn giá trị tìm được trong bước 2.

**Hint**: calculate the number of PGs for a pool at [here](https://ceph.com/pgcalc/)

### Replication
The default choice when creating a pool is _replicated_, meaning every object is copied on multiple disks.
```
ceph osd pool create {pool-name} {pg-num} [{pgp-num}] [replicated] \
     [crush-rule-name] [expected-num-objects]
```
Both Ceph OSD daemons and Ceph Clients use the **CRUSH** algorithm. The Ceph OSD Daemon uses CRUSH to compute where replicas of objects should be stored and for rebalancing. But Ceph Client uses the CRUSH algorithm to compute where to store an object, maps the object to a pool and placement group, then looks at the CRUSH map to identify the primary OSD for the placement group.

The below is the workflow when client writes an object to Ceph OSD:

giant![write_object](http://docs.ceph.com/docs/giant/_images/ditaa-54719cc959473e68a317f6578f9a2f0f3a8345ee.png)
**Giải thích:** sơ đồ client thực hiện lưu 1 **object** vào 1 **pool**:
- Đầu tiên, client sẽ sử dụng thuật toán **CRUSH** để xác định **PG** sẽ quản lý object đó, sau đó sẽ tìm kiếm **primary OSD** của PG đó trong **CRUSH map**.  
- Client gửi hành động ghi object tới **primary OSD** của PG đã được xác định từ bước trên. Sau đó, primary OSD này sẽ xác định các **replicas** dựa trên **bản copy CRUSH map** của nó, và gửi yêu cầu tạo replication cho object. (Việc các **OSDs** luôn có bản **updated** copy **CRUSH map** tham khảo tại [đây](http://docs.ceph.com/docs/giant/rados/configuration/mon-osd-interaction/))

### Erasure Code
Erasure coded pools store each object as `K+M` chunks, where `K` represents `data chunks` and `M` represents `coding chunks`. The sum `K+M` represents the number of OSDs used to store the object and the the `M` value represents the number of OSDs that can fail and still restore data should the `M` number of OSDs fail.
```
ceph osd pool create {pool-name} {pg-num}  {pgp-num}   erasure \
     [erasure-code-profile] [crush-rule-name] [expected_num_objects]
```
To check the default of a erasure pool:
```
ceph osd erasure-code-profile get default
k=2
m=1
plugin=jerasure
crush-failure-domain=host
technique=reed_sol_van
```
**Note**: can not modify the `profile` after the pool is created.

**E.g**
```
$ ceph osd erasure-code-profile set myprofile \
   k=3 m=2 \
   crush-failure-domain=rack
$ ceph osd pool create ecpool 12 12 erasure myprofile
$ echo ABCDEFGHI | rados --pool ecpool put NYAN -
$ rados --pool ecpool get NYAN -
ABCDEFGHI
```
![ceph_erasure_coded](http://docs.ceph.com/docs/mimic/_images/ditaa-96fe8c3c73e5e54cf27fa8a4d64ed08d17679ba3.png)

### Rebalancing

When you add a Ceph OSD Daemon to a Ceph Storage Cluster, the cluster map gets updated with the new OSD.

![rebalancing](http://docs.ceph.com/docs/mimic/_images/ditaa-b31e1f646135b9706000fa0799d572563dffac81.png)

Cơ chế: TODO
- Not all of the PGs migrate from existing OSDs (OSD 1, and OSD 2) to the new OSD (OSD 3)
- Even when rebalancing, CRUSH is stable.



### Data Stripping

**Problem**: Storage devices have throughput limitations => impact performance.
**Solution**: using **striping**: storing `sequential pieces` of data across `multiple storage devices`.
**In Ceph**: Ceph clients stripe the data over multiple Ceph Storage Cluster objects. Then Ceph clients will write directly to the Ceph Storage Cluster via `librados` must perform the striping (and parallel I/O) for themselves to obtain these benefits.
 
**1. The simplest Ceph stripping**	 
The simplest Ceph striping format involves a stripe count of 1 object. Ceph Clients write stripe units to a Ceph Storage Cluster object until the object is at its maximum capacity, and then create another object for additional stripes of data.

![simplest_ceph_stripping](https://access.redhat.com/webassets/avalon/d/Red_Hat_Ceph_Storage-1.2.3-Red_Hat_Ceph_Architecture-en-US/images/04e30dc99f20a2dad7b8cd2e27202238/diag-c2c1003ef2be507b5f086c43f19c2df0.png)

**2. Write tripped  data across an object set**

![using_object_set_for_ceph_stripping](https://access.redhat.com/webassets/avalon/d/Red_Hat_Ceph_Storage-1.2.3-Red_Hat_Ceph_Architecture-en-US/images/31d41d516b7f8de5fbb5ec2a96fce620/diag-7d2829e33d2c3709d5bc48bdb8b2c1e7.png)

Three important variables determine how Ceph client stripes data:
- **Object Size**: default = 4MB (min: 4Kb - max: 32MB)
- **Stripe Unit**: default = object size
- **Stripe Count**: default = 1

**NOTE**: `CANNOT change these striping parameters after you stripe the data and write it to objects.` => Test the performance of the striping configurations before applying into production.
Detail commands at [here](http://docs.ceph.com/docs/mimic/man/8/rbd/)

*Source: [here](https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/1.2.3/html/red_hat_ceph_architecture/ceph_client_architecture#)*
