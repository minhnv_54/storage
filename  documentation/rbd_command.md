## Một số lệnh rbd

Create image

```
rbd create --size 10G pool_name/image_name
```

List images

```
rbd ls pool_name
```

Show image info

```
rbd info pool_name/image_name
```

Resize image

```
rbd resize pool_name/image_name --size <size>
```

Remove image

```
rbd rm pool_name/image_name
```

Rename image

```
rbd mv pool_name/image_name pool_name/new_image_name
```

**Snapshot**

Create snapshot

```
rbd snap create pool_name/image_name@snap_name
```

List snapshot

```
rbd snap ls pool_name/image_name
```

Rollback Snapshot

```
rbd snap rollback pool_name/image_name@snap_name
```

Delete a snapshot

```
rbd snap rm pool_name/image_name@snap_name
```

Delete all snapshot in image

```
rbd snap purge pool_name/image_name
```

**Clone snapshot**

Để clone một bản snapshot thành 1 image trước tiên cần bảo vệ bản snapshot để ko được phép xóa bản snapshot này

```
rbd snap protect pool_name/image_name@snap_name
```

Clone snapshot

```
rbd clone pool_name/image_name@snap_name pool_name/image_name
```
