### 1. IOPS - Latency - Thoughput

**IOPS** (Inputs Outputs Per Second): tốc độ truy cập đọc hoặc viết mỗi giây.
Đây là chỉ số phổ biến để đo hiệu năng của các **thiết bị lữu trữ** như ổ đĩa cứng **SATA**, **SAS** và **ổ cứng SSD**. Đối với IOPS, thứ quan trọng nhất ta cần chú ý đến là tỉ lệ Read và Write

Một số hiệu suất thường được đo:
- IOPS đọc ngẫu nhiên (Random Read IOPS)  
- IOPS viết ngẫu nhiên IOPS (Random Write IOPS)  
- IOPS tuần tự đọc IOPS (Sequential Read IOPS)  
- IOPS tuần tự viết IOPS (Sequential Write IOPS)

**Latency**: độ trễ trung bình 	
**Throughput**: thông lượng 

Cách tính: 
		`IOPS = (MBps Throughput / KB per IO) * 1024`

Ví dụ: Ổ SSD ghi thông tin số lượng đọc ghi random 4k là 20,000 IOPS nghĩa là tốc độ đọc ghi trung bình sẽ là: (20000*4)/1024 = 78.125 MB/s

### 2. DAS - NAS - SAN - Object

![das-nas-san](https://image.slidesharecdn.com/ccloudorganizationalimpactsbigdataon-premvsoff-premisejohnsing-141020133243-conversion-gate02/95/c-cloud-organizationalimpactsbigdataonpremvsoffpremisejohnsing-45-638.jpg?cb=1413813141)

#### 2.1 DAS - Direct Attached Storage
DAS là dạng lưu trữ mà các storage devices được gắn trực tiếp vào các server. Các ứng dụng có thể truy cập vào ổ các ổ cứng này ở mức độ **block-level** hay **file-level**.
Các protocol được sử dụng cho DAS connections: 
- **ATA (AT Attachment)**: là chuẩn giao tiếp (interface standard) cho storage devices với server. Dưới đây là hình về chuẩn ATA	
- **SATA (Serial ATA)**: là **computer bus** interface that connects ***host bus adapters** to storage devices.
- eSATA
- NVMe
- **SCSI (Small Computer System Interface)**: là chuẩn để connect và transfer data giữa **computer** và **peripheral devices**. SCSI định nghĩa ra các: commands, protocols, electrical và logical interfaces.
- SAS
- USB
- USB 3.0

#### 2.2 NAS - Network Attached Storage
**NAS** là một mạng được thiết kế để kết nối các máy chủ tới hệ thống lưu trữ dữ liệu, trong đó các máy chủ truy cập tới hệ thống lưu trữ ở mức **file-level**.
Các protocol:
- **NFS** (unix) - Network File System
- **CIFS** (window) - Common Internet File System

#### 2.3 SAN - Storage Area Network
**SAN** là một mạng được thiết kế để kết nối các máy chủ tới hệ thống lưu trữ dữ liệu, trong đó các máy chủ truy cập tới hệ thống lưu trữ ở mức **block-level**.
Các protocol:
- **iSCSI** (internet SCSI)
- **FC** (Fiber Channel)

####  2.4 SDS - Software-defined Storage
**SDS** là định nghĩa chỉ các phần mềm giúp giảm TCO (Total Cost of Ownership) cho hệ thống lưu trữ dữ liệu (storage infrastructure)
![TCO](http://www.reliant-technology.com/media/wysiwyg/blog_img/TCO_1.jpg)

Các hằng số cần tính toán cho TCO khi xây dựng 1 hệ thống lưu trữ
Source: http://www.storage-switzerland.com/Articles/Entries/2012/1/9_The_TCO_Problem_of_Storage.html 

1. Tính toán dung lượng cần thiết (calculatin upfont capacity requirements): dung lượng để cho các ứng dụng sử dụng ban đầu (day one), và tính toán xu hướng sử dụng storage của các ứng dụng đó.
	- E.g: An example might be a file-sharing application, which would require an upfront capacity of **25TB** on day one, with the expectation to grow to 100 TB within the first year as it gets fully rolled out, followed by a growth of **50% per year**. Then, if the storage is expected to last four years a total required capacity of **340TB** should be expected. This capacity though is just the beginning and represents the “acquisition cost” not the “Total Cost Of Ownership” which the remaining steps will explain.
 2. Tính toán dung lượng cần thiết thực sự (Calculating true capacity requirements)
- Đầu tiên ta cần tính toán dung lượng cần thiết thực sự để các ứng dụng có thể truy xuất dữ liệu với hiệu suất tốt nhất. Thông số trong mục 1 mới chỉ là dung lượng lưu trữ cần thiết để cho các ứng dụng lưu trữ dữ liệu. Trong thực tế, đặc biệt là các ứng dụng hỗ trợ **unstructured data**, sẽ có xu hướng bị tổn thất hiệu suất đáng kể khi ứng dụng bắt đầu tiêu thụ tỉ lệ phần trăm cao hơn tổng dung lượng có sẵn. Điều này phụ thuộc rất nhiều vào hiệu năng của hệ thống lưu trữ. Ví dụ đối với các hệ thốn sử dụng **SAN** hoặc **NAS** thường được recommend: tổng dung lượng lưu trữ cấp cho các ứng dụng sử dụng không bao giờ được lớn hơn **60% (???)** tổng dung lượng của hệ thống.
	- Trong ví dụ mục 1, như vậy, với việc các ứng dụng cần 340TB thì ta sẽ cần có hệ thống lưu trữ với dung lượng là 564TB (x60%)
- Thứ 2, ta cần xem xét tới tác nhân phần cứng là các ổ cứng, trong trường hợp hỏng ổ cứng gây mất dữ liệu, do đó cần có phương pháp phòng chống. Có 2 phương pháp thường sử dụng đó là: **online volume data protection** sử dụng **RAID** và **off-line volume protection** khi sử dụng **backup copies**.
	- RAID được sử dụng tùy thuộc vào mức độ bảo vệ cần thiết của dữ liệu (RAID-0, 1, 5, 6, 10) mà sẽ cần thêm dung lượng là bao nhiêu, nhưng thông thường sẽ là thêm 30% tổng dung lượng ổ cứng cần có. Do đó, ứng dụng trong ví dụ 1 sẽ cần: 564TB *1.3 = 733TB.
	- Khi cần **backup data**, thông thường ta cần tối thiểu là mỗi volume sẽ có 1 bản copy, do đó cần thêm 100% dung lượng lưu trữ mà các ứng dụng sử dụng, và có nhiều khả năng ở mức 200% khi cần thêm các bản sao lưu. Trong ví dụ ở mục 1, giả sử ta cần thêm 150% để backup data, nghĩa là cần thêm: 340TB *1.5 = 510TB nữa. Bây giờ, tổng dung lượng mà ứng dụng trong ví dụ cần sử dụng là: 733TB + 510TB = 1243TB ~ **1.2PB**
3. Tính toán chi phí tuổi thọ (Calculating lifespan costs)
Cuối cùng ta cần tính toán tuổi thọ của các ứng dụng sử dụng hệ thống lưu trữ cũng như tuổi thọ của hệ thống lưu trữ đó, để migrate dữ liệu, ...

**RAID - the end of an era**
- Quá trình rebuild của RAID max lâu: ví dụ, với các ổ cứng có dung lượng **4-6TB**, để khôi phục lại dữ liệu sử dụng RAID sẽ mất **several hours up to days**, trong khi công nghệ tạo ra ổ cứng với dung lượng lớn tăng nhanh theo từng năm, e.g 450Gb, 600Gb, 1TB, ...
- Sử dụng RAID làm tăng chí phí **TCO**: RAID cần có các disk để trống để sử dụng nó khôi phục lại dữ liệu khi có ổ disk đang sử dụng chết => Các ổ cứng trống (free disks) sẽ không được sử dụng cho tới khi có 1 ổ đang sử dụng chết.
- Sử dụng RAID làm tăng chi phí hardware: "RAID requires a set of identical disk drivers in a single RAID group; you would face penalties if you change the disk size, rpm, or disk type." => **Ceph** is a software-defined storage, so we do not require any specialized hardware for data replication
- The growing RAID group is a challenge
- The RAID reliability model is no longer promising: RAID5 và RAID6 là 2 loại hay được sử dụng nhất, nhưng sẽ không khôi phục được dữ liệu khi có 2 ổ cứng hỏng => The biggest drawbacks of RAID systems. => **Ceph** makes use of the data replication method, which means it does not use RAID.

#### Tài liệu tham khảo
- Learning Ceph (second edition)
