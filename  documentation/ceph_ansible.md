# Sử dụng ceph-ansible triển khai ceph

## Mô hình

![image](/uploads/f2262d50bd68152500dae99ac1237f99/image.png)

3 node trên cài cả ceph-mon và ceph-osd

## Cài đặt

Tạo môi trường ảo

```
cd /root/virtualenv/
virtualenv ceph-ansible -p python3
source /root/virtualenv/ceph-ansible/bin/activate
pip install ansible==2.9
```

Clone repo

```
cd /root/git
git clone http://10.240.203.2:8180/cloud-team/cloud-2020/ceph-ansible.git
```

Copy ssh-key

```
ssh-keygen
ssh-copy-id cephuser@10.240.201.235
ssh-copy-id cephuser@10.240.201.236
ssh-copy-id cephuser@10.240.201.237
```

Config SSH: Update /root/.ssh/config

```
cat <<EOF >> /root/.ssh/config
Host ceph235
    Hostname 10.240.201.235
    User cephuser

Host ceph236
    Hostname 10.240.201.236
    User cephuser

Host ceph237
    Hostname 10.240.201.237
    User cephuser
EOF
```

Tạo file site.yml

```
cd /root/git/ceph-ansible
cp site.yml.sample  site.yml
```

Tạo file all.yml với nội dung như sau

```
cd /root/git/ceph-ansible/group_vars
(ceph-ansible) [root@controller235 group_vars]# cat all.yml | egrep -v "^#|^$"
---
dummy:
ceph_stable_release: octopus
fetch_directory: /etc/ceph
cluster: ceph
configure_firewall: false
debian_package_dependencies: []
ntp_service_enabled: false
ceph_repository_type: cdn
ceph_origin: repository
ceph_repository: custom
ceph_custom_key: http://10.60.129.132/repository/Ceph_Keys/keys/release.asc
ceph_custom_repo: http://10.60.129.132/repository/Ceph_Keys/repos/ceph-octopus.repo
monitor_interface: bond0.651
public_network: 10.240.201.0/24
cluster_network: 10.4.4.0/24
dashboard_enabled: false
ceph_conf_overrides:
    global:
        mon_allow_pool_delete: true
```

Tạo file osds.yml(có 4 trường hợp khai báo file osds.yml) và inventory file

* **TH1: Disk trên osd node là raw disk và tên các disk trên các node là giống nhau**

File osds.yml

```
cd /root/git/ceph-ansible/group_vars
(ceph-ansible) [root@controller235 group_vars]# cat osds.yml
---
dummy:
osd_scenario: collocated
osd_objectstore: bluestore
devices:
  - /dev/sdb
  - /dev/sdc
  - /dev/sdd
  - /dev/sde
crush_rule_config: true
crush_rule_hdd:
  name: generic_hdd
  root: default
  type: host
  class: hdd
  default: false
crush_rules:
  - "{{ crush_rule_hdd }}"
```

File inventory

```
(ceph-ansible) [root@controller235 group_vars]#cat /root/inventory/ceph_octopus
[mons]
ceph235
ceph236
ceph237

[osds]
ceph235
ceph236
ceph237
```

* **TH2: Disk trên osd node là raw disk và tên các disk trên các node là khác nhau**

File osds.yml

```
cd /root/git/ceph-ansible/group_vars
(ceph-ansible) [root@controller235 group_vars]# cat osds.yml
---
dummy:
osd_scenario: collocated
osd_objectstore: bluestore
crush_rule_config: true
crush_rule_hdd:
  name: generic_hdd
  root: default
  type: host
  class: hdd
  default: false
crush_rules:
  - "{{ crush_rule_hdd }}"
```

File inventory

```
(ceph-ansible) [root@controller235 group_vars]#cat /root/inventory/ceph_octopus
[mons]
ceph235
ceph236
ceph237

[osds]
ceph235 devices=['/dev/sda', '/dev/sdb', '/dev/sdc']
ceph236 devices=['/dev/sdb', '/dev/sdc', '/dev/sdd']
ceph237 devices=['/dev/sda', '/dev/sdc', '/dev/sdd']
```

* **TH3: Disk trên osd node là device mapper và tên các lv trên các node là giống nhau**

Nếu là device mapper thì ta cần tạo lv trên các các osd host trước (bước này vào từng osd host và sử dụng pvcreate, vgcreate, lvcreate để tạo)

File osds.yml

```
cd /root/git/ceph-ansible/group_vars
(ceph-ansible) [root@controller235 group_vars]# cat osds.yml
---
dummy:
osd_scenario: collocated
osd_objectstore: bluestore
lvm_volumes:
  - data: lv_osd_01
    data_vg: vg_osd_01
  - data: lv_osd_02
    data_vg: vg_osd_02
crush_rule_config: true
crush_rule_hdd:
  name: generic_hdd
  root: default
  type: host
  class: hdd
  default: false
crush_rules:
  - "{{ crush_rule_hdd }}"
```

File inventory

```
(ceph-ansible) [root@controller235 group_vars]#cat /root/inventory/ceph_octopus
[mons]
ceph235
ceph236
ceph237

[osds]
ceph235
ceph236
ceph237
```

* **TH4: Disk trên osd node là device mapper và tên các lv trên các node là khác nhau**

Device mapper thì ta cần tạo lv trên các các osd host trước (bước này vào từng osd host và sử dụng pvcreate, vgcreate, lvcreate để tạo)

File osds.yml

```
cd /root/git/ceph-ansible/group_vars
(ceph-ansible) [root@controller235 group_vars]# cat osds.yml
---
dummy:
osd_scenario: collocated
osd_objectstore: bluestore
crush_rule_config: true
crush_rule_hdd:
  name: generic_hdd
  root: default
  type: host
  class: hdd
  default: false
crush_rules:
  - "{{ crush_rule_hdd }}"
```

File inventory

```
(ceph-ansible) [root@controller235 group_vars]#cat /root/inventory/ceph_octopus
[mons]
ceph235
ceph236
ceph237

[osds]
ceph235 lvm_volumes="[{'data': 'lv_osd235_01', 'data_vg': 'vg_osd235_01'}, {'data': 'lv_osd235_02', 'data_vg': 'vg_osd235_02'}]"
ceph236 lvm_volumes="[{'data': 'lv_osd236_01', 'data_vg': 'vg_osd236_01'}, {'data': 'lv_osd236_02', 'data_vg': 'vg_osd236_02'}]"
ceph237 lvm_volumes="[{'data': 'lv_osd237_01', 'data_vg': 'vg_osd237_01'}, {'data': 'lv_osd237_02', 'data_vg': 'vg_osd237_02'}]"
```

Ok chạy và đợi (Hy vọng là ko có lỗi nếu lỗi thì xử lý tiếp)

```
cd /root/git/ceph-ansible
ansible-playbook -i /root/inventory/ceph_octopus site.yml
```
