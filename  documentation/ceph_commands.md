# Ceph commands 

### Ceph health:

	# ceph -s or # ceph status
	# ceph health detail
	# ceph -w
    # ceph mon stat
    # ceph osd stat
    # ceph osd pool stats
    # ceph pg stat

### Ceph Monitor

	 # ceph mon dump
	 # ceph quorum_status --format json-pretty

### Ceph cluster

	 # ceph df
	 # rados df

### Ceph Placement group:

	 # ceph pg dump
	 # rados list-inconsistent-pg <pool>
	 # ceph pg [ debug | deep-scrub | dump | dump_json | dump_pools_json | dump_stuck | getmap | ls | ls-by-osd | ls-by-pool | ls-by-primary | map | repair | scrub | stat ] <pd id>

`Count the number of PGs per OSD`

	 # ceph pg dump | awk ' BEGIN { IGNORECASE = 1 } /^PG_STAT/ { col=1; while($col!="UP") {col++}; col++ } /^[0-9a-f]+\.[0-9a-f]+/ { match($0,/^[0-9a-f]+/); pool=substr($0, RSTART, RLENGTH); poollist[pool]=0; up=$col; i=0; RSTART=0; RLENGTH=0; delete osds; while(match(up,/[0-9]+/)>0) { osds[++i]=substr(up,RSTART,RLENGTH); up = substr(up, RSTART+RLENGTH) } for(i in osds) {array[osds[i],pool]++; osdlist[osds[i]];} } END { printf("\n"); printf("pool :\t"); for (i in poollist) printf("%s\t",i); printf("| SUM \n"); for (i in poollist) printf("--------"); printf("----------------\n"); for (i in osdlist) { printf("osd.%i\t", i); sum=0; for (j in poollist) { printf("%i\t", array[i,j]); sum+=array[i,j]; sumpool[j]+=array[i,j] }; printf("| %i\n",sum) } for (i in poollist) printf("--------"); printf("----------------\n"); printf("SUM :\t"); for (i in poollist) printf("%s\t",sumpool[i]); printf("|\n"); }'


### Ceph Pool:

	 # ceph osd pool ls detail
	 # ceph pg ls-by-pool <pool> active
	 # rados lspools

### Ceph Rados Object

    # rados list-inconsistent-obj <pg id>


### Ceph cluster authentication:

	# ceph auth list

### Ceph OSD

Check OSD
    
    # ceph osd tree
    # ceph osd df
    ```
    
Add OSD

    $ for i in {0..33..3}; do systemctl stop ceph-osd@$i;done;
    $ for i in {0..33..3}; do ceph osd out osd.$i;done
    $ for i in {0..33..3}; do ceph osd purge osd.$i --yes-i-really-mean-it;done;

Delete OSD

    `TODO`

**Exercise: Locate an Object**
```
rados put {object-name} {file-path} --pool=data
rados put test-object-1 testfile.txt --pool=data
```
To verify that the Ceph Object Store stored the object, execute the following:
```
rados -p data ls
```
Now, identify the object location:
```
ceph osd map {pool-name} {object-name}
ceph osd map data test-object-1
```
Ceph should output the object’s location. For example:
```
osdmap e537 pool 'data' (0) object 'test-object-1' -> pg 0.d1743484 (0.4) -> up ([1,0], p0) acting ([1,0], p0)
```
To remove the test object, simply delete it using the  `radosrm`  command. For example:
```
rados rm test-object-1 --pool=data
```
